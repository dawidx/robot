-- Vhdl test bench created from schematic H:\Robot\Robot\decoder.sch - Fri May 10 11:07:58 2013
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY decoder_decoder_sch_tb IS
END decoder_decoder_sch_tb;
ARCHITECTURE behavioral OF decoder_decoder_sch_tb IS 

   COMPONENT decoder
   PORT( LM2	:	OUT	STD_LOGIC; 
          RM1	:	OUT	STD_LOGIC; 
          I2	:	IN	STD_LOGIC; 
          LM1	:	OUT	STD_LOGIC; 
          I0	:	IN	STD_LOGIC; 
          I1	:	IN	STD_LOGIC; 
          RM2	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL LM2	:	STD_LOGIC;
   SIGNAL RM1	:	STD_LOGIC;
   SIGNAL I2	:	STD_LOGIC;
   SIGNAL LM1	:	STD_LOGIC;
   SIGNAL I0	:	STD_LOGIC;
   SIGNAL I1	:	STD_LOGIC;
   SIGNAL RM2	:	STD_LOGIC;

BEGIN

   UUT: decoder PORT MAP(
		LM1 => LM1,
		LM2 => LM2, 
		RM1 => RM1, 
		RM2 => RM2, 
		I0 => I0, 
		I1 => I1,
		I2 => I2 
   );
	
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		I0 <= '1';
		I1 <= '1';
		I2 <= '1';
		WAIT FOR 100ns;
		I0 <= '1';
		I1 <= '1';
		I2 <= '0';
		WAIT FOR 100ns;
		I0 <= '1';
		I1 <= '0';
		I2 <= '1';
		WAIT FOR 100ns;
		I0 <= '1';
		I1 <= '0';
		I2 <= '0';
		WAIT FOR 100ns;
		I0 <= '0';
		I1 <= '1';
		I2 <= '1';
		WAIT FOR 100ns;
		I0 <= '0';
		I1 <= '1';
		I2 <= '0';
		WAIT FOR 100ns;
		I0 <= '0';
		I1 <= '0';
		I2 <= '1';
		WAIT FOR 100ns;
		I0 <= '0';
		I1 <= '0';
		I2 <= '0';
		WAIT FOR 100ns;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
