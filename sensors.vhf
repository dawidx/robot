--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : sensors.vhf
-- /___/   /\     Timestamp : 05/10/2013 13:11:38
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Robot/sensors.vhf -w H:/Robot/Robot/sensors.sch
--Design Name: sensors
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD_MXILINX_sensors is
   generic( INIT : bit :=  '0');
   port ( C : in    std_logic; 
          D : in    std_logic; 
          Q : out   std_logic);
end FD_MXILINX_sensors;

architecture BEHAVIORAL of FD_MXILINX_sensors is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_43 : GND
      port map (G=>XLXN_4);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>XLXN_4,
                D=>D,
                PRE=>XLXN_4,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity XOR6_MXILINX_sensors is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          O  : out   std_logic);
end XOR6_MXILINX_sensors;

architecture BEHAVIORAL of XOR6_MXILINX_sensors is
   attribute BOX_TYPE   : string ;
   signal I35 : std_logic;
   component XOR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR4 : component is "BLACK_BOX";
   
   component XOR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR3 : component is "BLACK_BOX";
   
begin
   I_36_87 : XOR4
      port map (I0=>I0,
                I1=>I1,
                I2=>I2,
                I3=>I35,
                O=>O);
   
   I_36_88 : XOR3
      port map (I0=>I3,
                I1=>I4,
                I2=>I5,
                O=>I35);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1_MXILINX_sensors is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1_MXILINX_sensors;

architecture BEHAVIORAL of M2_1_MXILINX_sensors is
   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
begin
   I_36_7 : AND2B1
      port map (I0=>S0,
                I1=>D0,
                O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1,
                I1=>S0,
                O=>M1);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity sensors is
   port ( B0       : in    std_logic; 
          B1       : in    std_logic; 
          B2       : in    std_logic; 
          B3       : in    std_logic; 
          B4       : in    std_logic; 
          B5       : in    std_logic; 
          CLK      : in    std_logic; 
          DATA_OUT : out   std_logic);
end sensors;

architecture BEHAVIORAL of sensors is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_14  : std_logic;
   signal XLXN_28  : std_logic;
   signal XLXN_29  : std_logic;
   signal XLXN_30  : std_logic;
   signal XLXN_31  : std_logic;
   signal XLXN_33  : std_logic;
   signal XLXN_34  : std_logic;
   signal XLXN_35  : std_logic;
   signal XLXN_36  : std_logic;
   signal XLXN_47  : std_logic;
   signal XLXN_48  : std_logic;
   signal XLXN_49  : std_logic;
   signal XLXN_50  : std_logic;
   signal XLXN_51  : std_logic;
   signal XLXN_54  : std_logic;
   signal XLXN_95  : std_logic;
   signal XLXN_97  : std_logic;
   signal XLXN_98  : std_logic;
   signal XLXN_99  : std_logic;
   signal XLXN_100 : std_logic;
   signal XLXN_101 : std_logic;
   signal XLXN_104 : std_logic;
   signal XLXN_106 : std_logic;
   signal XLXN_108 : std_logic;
   signal XLXN_110 : std_logic;
   signal XLXN_112 : std_logic;
   signal XLXN_113 : std_logic;
   signal XLXN_115 : std_logic;
   signal XLXN_129 : std_logic;
   signal XLXN_138 : std_logic;
   signal XLXN_139 : std_logic;
   signal XLXN_141 : std_logic;
   signal XLXN_142 : std_logic;
   signal XLXN_143 : std_logic;
   signal XLXN_144 : std_logic;
   signal XLXN_145 : std_logic;
   signal XLXN_146 : std_logic;
   signal XLXN_147 : std_logic;
   signal XLXN_149 : std_logic;
   signal XLXN_157 : std_logic;
   signal XLXN_158 : std_logic;
   signal XLXN_159 : std_logic;
   signal XLXN_160 : std_logic;
   signal XLXN_161 : std_logic;
   signal XLXN_178 : std_logic;
   signal XLXN_179 : std_logic;
   component FD_MXILINX_sensors
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component M2_1_MXILINX_sensors
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component XOR6_MXILINX_sensors
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component NAND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND4 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_9 : label is "XLXI_9_7";
   attribute HU_SET of XLXI_10 : label is "XLXI_10_6";
   attribute HU_SET of XLXI_11 : label is "XLXI_11_5";
   attribute HU_SET of XLXI_12 : label is "XLXI_12_4";
   attribute HU_SET of XLXI_13 : label is "XLXI_13_3";
   attribute HU_SET of XLXI_14 : label is "XLXI_14_2";
   attribute HU_SET of XLXI_15 : label is "XLXI_15_1";
   attribute HU_SET of XLXI_16 : label is "XLXI_16_0";
   attribute HU_SET of XLXI_17 : label is "XLXI_17_13";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_12";
   attribute HU_SET of XLXI_19 : label is "XLXI_19_11";
   attribute HU_SET of XLXI_20 : label is "XLXI_20_10";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_9";
   attribute HU_SET of XLXI_22 : label is "XLXI_22_8";
   attribute HU_SET of XLXI_23 : label is "XLXI_23_14";
   attribute HU_SET of XLXI_40 : label is "XLXI_40_15";
   attribute HU_SET of XLXI_41 : label is "XLXI_41_16";
   attribute HU_SET of XLXI_42 : label is "XLXI_42_17";
   attribute HU_SET of XLXI_43 : label is "XLXI_43_18";
   attribute HU_SET of XLXI_44 : label is "XLXI_44_19";
   attribute HU_SET of XLXI_45 : label is "XLXI_45_20";
   attribute HU_SET of XLXI_46 : label is "XLXI_46_21";
   attribute HU_SET of XLXI_47 : label is "XLXI_47_22";
   attribute HU_SET of XLXI_48 : label is "XLXI_48_23";
   attribute HU_SET of XLXI_49 : label is "XLXI_49_24";
   attribute HU_SET of XLXI_50 : label is "XLXI_50_25";
   attribute HU_SET of XLXI_51 : label is "XLXI_51_26";
   attribute HU_SET of XLXI_52 : label is "XLXI_52_27";
   attribute HU_SET of XLXI_53 : label is "XLXI_53_28";
   attribute HU_SET of XLXI_54 : label is "XLXI_54_29";
   attribute HU_SET of XLXI_56 : label is "XLXI_56_30";
   attribute HU_SET of XLXI_63 : label is "XLXI_63_31";
   attribute HU_SET of XLXI_64 : label is "XLXI_64_32";
   attribute HU_SET of XLXI_65 : label is "XLXI_65_33";
   attribute HU_SET of XLXI_66 : label is "XLXI_66_34";
begin
   XLXI_9 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_145,
                Q=>XLXN_28);
   
   XLXI_10 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_14,
                Q=>XLXN_29);
   
   XLXI_11 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_47,
                Q=>XLXN_30);
   
   XLXI_12 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_48,
                Q=>XLXN_31);
   
   XLXI_13 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_49,
                Q=>XLXN_33);
   
   XLXI_14 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_50,
                Q=>XLXN_34);
   
   XLXI_15 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_51,
                Q=>XLXN_35);
   
   XLXI_16 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_54,
                Q=>XLXN_36);
   
   XLXI_17 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_145,
                D1=>XLXN_28,
                S0=>XLXN_179,
                O=>XLXN_14);
   
   XLXI_18 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_141,
                D1=>XLXN_29,
                S0=>XLXN_179,
                O=>XLXN_47);
   
   XLXI_19 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_142,
                D1=>XLXN_30,
                S0=>XLXN_179,
                O=>XLXN_48);
   
   XLXI_20 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_143,
                D1=>XLXN_31,
                S0=>XLXN_179,
                O=>XLXN_49);
   
   XLXI_21 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_144,
                D1=>XLXN_33,
                S0=>XLXN_179,
                O=>XLXN_50);
   
   XLXI_22 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_139,
                D1=>XLXN_34,
                S0=>XLXN_179,
                O=>XLXN_51);
   
   XLXI_23 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_138,
                D1=>XLXN_35,
                S0=>XLXN_179,
                O=>XLXN_54);
   
   XLXI_40 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_115,
                Q=>DATA_OUT);
   
   XLXI_41 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_112,
                Q=>XLXN_113);
   
   XLXI_42 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_110,
                Q=>XLXN_97);
   
   XLXI_43 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_108,
                Q=>XLXN_98);
   
   XLXI_44 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_106,
                Q=>XLXN_99);
   
   XLXI_45 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_104,
                Q=>XLXN_100);
   
   XLXI_46 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_95,
                Q=>XLXN_101);
   
   XLXI_47 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_36,
                Q=>XLXN_178);
   
   XLXI_48 : M2_1_MXILINX_sensors
      port map (D0=>B5,
                D1=>XLXN_97,
                S0=>XLXN_179,
                O=>XLXN_112);
   
   XLXI_49 : M2_1_MXILINX_sensors
      port map (D0=>B4,
                D1=>XLXN_98,
                S0=>XLXN_179,
                O=>XLXN_110);
   
   XLXI_50 : M2_1_MXILINX_sensors
      port map (D0=>B3,
                D1=>XLXN_99,
                S0=>XLXN_179,
                O=>XLXN_108);
   
   XLXI_51 : M2_1_MXILINX_sensors
      port map (D0=>B2,
                D1=>XLXN_100,
                S0=>XLXN_179,
                O=>XLXN_106);
   
   XLXI_52 : M2_1_MXILINX_sensors
      port map (D0=>B1,
                D1=>XLXN_101,
                S0=>XLXN_179,
                O=>XLXN_104);
   
   XLXI_53 : M2_1_MXILINX_sensors
      port map (D0=>B0,
                D1=>XLXN_178,
                S0=>XLXN_179,
                O=>XLXN_95);
   
   XLXI_54 : M2_1_MXILINX_sensors
      port map (D0=>XLXN_129,
                D1=>XLXN_113,
                S0=>XLXN_179,
                O=>XLXN_115);
   
   XLXI_55 : GND
      port map (G=>XLXN_129);
   
   XLXI_56 : XOR6_MXILINX_sensors
      port map (I0=>B5,
                I1=>B4,
                I2=>B3,
                I3=>B2,
                I4=>B1,
                I5=>B0,
                O=>XLXN_138);
   
   XLXI_57 : VCC
      port map (P=>XLXN_139);
   
   XLXI_58 : VCC
      port map (P=>XLXN_144);
   
   XLXI_59 : VCC
      port map (P=>XLXN_143);
   
   XLXI_60 : VCC
      port map (P=>XLXN_142);
   
   XLXI_61 : VCC
      port map (P=>XLXN_141);
   
   XLXI_62 : VCC
      port map (P=>XLXN_145);
   
   XLXI_63 : FD_MXILINX_sensors
      port map (C=>CLK,
                D=>XLXN_147,
                Q=>XLXN_146);
   
   XLXI_64 : FD_MXILINX_sensors
      port map (C=>XLXN_146,
                D=>XLXN_157,
                Q=>XLXN_149);
   
   XLXI_65 : FD_MXILINX_sensors
      port map (C=>XLXN_149,
                D=>XLXN_159,
                Q=>XLXN_158);
   
   XLXI_66 : FD_MXILINX_sensors
      port map (C=>XLXN_158,
                D=>XLXN_160,
                Q=>XLXN_161);
   
   XLXI_68 : INV
      port map (I=>XLXN_146,
                O=>XLXN_147);
   
   XLXI_69 : INV
      port map (I=>XLXN_149,
                O=>XLXN_157);
   
   XLXI_70 : INV
      port map (I=>XLXN_158,
                O=>XLXN_159);
   
   XLXI_71 : INV
      port map (I=>XLXN_161,
                O=>XLXN_160);
   
   XLXI_74 : NAND4
      port map (I0=>XLXN_146,
                I1=>XLXN_149,
                I2=>XLXN_158,
                I3=>XLXN_161,
                O=>XLXN_179);
   
end BEHAVIORAL;


