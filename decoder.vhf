--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : decoder.vhf
-- /___/   /\     Timestamp : 05/10/2013 13:11:38
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Robot/decoder.vhf -w H:/Robot/Robot/decoder.sch
--Design Name: decoder
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity decoder is
   port ( I0  : in    std_logic; 
          I1  : in    std_logic; 
          I2  : in    std_logic; 
          LM1 : out   std_logic; 
          LM2 : out   std_logic; 
          RM1 : out   std_logic; 
          RM2 : out   std_logic);
end decoder;

architecture BEHAVIORAL of decoder is
   attribute BOX_TYPE   : string ;
   signal XLXN_5    : std_logic;
   signal XLXN_9    : std_logic;
   signal XLXN_13   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_18   : std_logic;
   signal XLXN_19   : std_logic;
   signal XLXN_21   : std_logic;
   signal XLXN_24   : std_logic;
   signal XLXN_25   : std_logic;
   signal XLXN_26   : std_logic;
   signal XLXN_30   : std_logic;
   signal RM1_DUMMY : std_logic;
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   RM1 <= RM1_DUMMY;
   XLXI_2 : INV
      port map (I=>I0,
                O=>XLXN_9);
   
   XLXI_3 : AND2
      port map (I0=>XLXN_5,
                I1=>XLXN_9,
                O=>LM2);
   
   XLXI_4 : OR2
      port map (I0=>XLXN_24,
                I1=>RM1_DUMMY,
                O=>XLXN_5);
   
   XLXI_5 : INV
      port map (I=>I2,
                O=>XLXN_24);
   
   XLXI_6 : INV
      port map (I=>I1,
                O=>RM1_DUMMY);
   
   XLXI_7 : XOR2
      port map (I0=>I2,
                I1=>I1,
                O=>XLXN_13);
   
   XLXI_8 : AND2
      port map (I0=>XLXN_13,
                I1=>I0,
                O=>XLXN_14);
   
   XLXI_9 : OR2
      port map (I0=>XLXN_14,
                I1=>XLXN_18,
                O=>LM1);
   
   XLXI_11 : AND2
      port map (I0=>XLXN_21,
                I1=>XLXN_19,
                O=>XLXN_18);
   
   XLXI_12 : INV
      port map (I=>I0,
                O=>XLXN_19);
   
   XLXI_13 : XOR2
      port map (I0=>I2,
                I1=>RM1_DUMMY,
                O=>XLXN_21);
   
   XLXI_14 : AND2
      port map (I0=>XLXN_30,
                I1=>XLXN_24,
                O=>RM2);
   
   XLXI_15 : OR2
      port map (I0=>XLXN_25,
                I1=>XLXN_26,
                O=>XLXN_30);
   
   XLXI_16 : AND2
      port map (I0=>I1,
                I1=>I0,
                O=>XLXN_25);
   
   XLXI_17 : INV
      port map (I=>I0,
                O=>XLXN_26);
   
end BEHAVIORAL;


