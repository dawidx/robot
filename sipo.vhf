--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : sipo.vhf
-- /___/   /\     Timestamp : 05/10/2013 13:11:39
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Robot/sipo.vhf -w H:/Robot/Robot/sipo.sch
--Design Name: sipo
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD_MXILINX_sipo is
   generic( INIT : bit :=  '0');
   port ( C : in    std_logic; 
          D : in    std_logic; 
          Q : out   std_logic);
end FD_MXILINX_sipo;

architecture BEHAVIORAL of FD_MXILINX_sipo is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_43 : GND
      port map (G=>XLXN_4);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>XLXN_4,
                D=>D,
                PRE=>XLXN_4,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity sipo is
   port ( CLOCK     : in    std_logic; 
          SERIAL_IN : in    std_logic; 
          Q0        : out   std_logic; 
          Q1        : out   std_logic; 
          Q2        : out   std_logic);
end sipo;

architecture BEHAVIORAL of sipo is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_139  : std_logic;
   signal XLXN_192  : std_logic;
   signal XLXN_194  : std_logic;
   signal XLXN_199  : std_logic;
   signal XLXN_201  : std_logic;
   signal XLXN_202  : std_logic;
   signal XLXN_205  : std_logic;
   signal XLXN_209  : std_logic;
   signal XLXN_211  : std_logic;
   signal XLXN_212  : std_logic;
   signal XLXN_213  : std_logic;
   signal XLXN_214  : std_logic;
   signal XLXN_215  : std_logic;
   component FD_MXILINX_sipo
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component XOR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR3 : component is "BLACK_BOX";
   
   component XNOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XNOR2 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_2 : label is "XLXI_2_35";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_36";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_37";
   attribute HU_SET of XLXI_5 : label is "XLXI_5_38";
   attribute HU_SET of XLXI_35 : label is "XLXI_35_39";
   attribute HU_SET of XLXI_36 : label is "XLXI_36_40";
   attribute HU_SET of XLXI_37 : label is "XLXI_37_41";
   attribute HU_SET of XLXI_38 : label is "XLXI_38_42";
   attribute HU_SET of XLXI_40 : label is "XLXI_40_43";
   attribute HU_SET of XLXI_41 : label is "XLXI_41_44";
   attribute HU_SET of XLXI_42 : label is "XLXI_42_45";
begin
   XLXI_2 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>SERIAL_IN,
                Q=>XLXN_199);
   
   XLXI_3 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_199,
                Q=>XLXN_201);
   
   XLXI_4 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_201,
                Q=>XLXN_202);
   
   XLXI_5 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_202,
                Q=>XLXN_214);
   
   XLXI_35 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_214,
                Q=>XLXN_139);
   
   XLXI_36 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_139,
                Q=>XLXN_213);
   
   XLXI_37 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_213,
                Q=>XLXN_215);
   
   XLXI_38 : FD_MXILINX_sipo
      port map (C=>CLOCK,
                D=>XLXN_215,
                Q=>XLXN_205);
   
   XLXI_40 : FD_MXILINX_sipo
      port map (C=>XLXN_192,
                D=>XLXN_215,
                Q=>Q2);
   
   XLXI_41 : FD_MXILINX_sipo
      port map (C=>XLXN_192,
                D=>XLXN_213,
                Q=>Q1);
   
   XLXI_42 : FD_MXILINX_sipo
      port map (C=>XLXN_192,
                D=>XLXN_139,
                Q=>Q0);
   
   XLXI_60 : AND3
      port map (I0=>XLXN_199,
                I1=>XLXN_201,
                I2=>XLXN_202,
                O=>XLXN_194);
   
   XLXI_61 : AND3
      port map (I0=>XLXN_194,
                I1=>XLXN_209,
                I2=>XLXN_211,
                O=>XLXN_192);
   
   XLXI_62 : INV
      port map (I=>XLXN_205,
                O=>XLXN_209);
   
   XLXI_64 : XOR3
      port map (I0=>XLXN_215,
                I1=>XLXN_213,
                I2=>XLXN_139,
                O=>XLXN_212);
   
   XLXI_80 : XNOR2
      port map (I0=>XLXN_214,
                I1=>XLXN_212,
                O=>XLXN_211);
   
end BEHAVIORAL;


