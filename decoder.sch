<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="LM2" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="RM1" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="I2" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="LM1" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="I0" />
        <signal name="XLXN_28" />
        <signal name="I1" />
        <signal name="XLXN_30" />
        <signal name="RM2" />
        <signal name="XLXN_32" />
        <port polarity="Output" name="LM2" />
        <port polarity="Output" name="RM1" />
        <port polarity="Input" name="I2" />
        <port polarity="Output" name="LM1" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="I1" />
        <port polarity="Output" name="RM2" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="I0" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="LM2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="RM1" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="I2" name="I" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="I1" name="I" />
            <blockpin signalname="RM1" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_7">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="I1" name="I1" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="XLXN_13" name="I0" />
            <blockpin signalname="I0" name="I1" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_9">
            <blockpin signalname="XLXN_14" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="LM1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="XLXN_21" name="I0" />
            <blockpin signalname="XLXN_19" name="I1" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_12">
            <blockpin signalname="I0" name="I" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_13">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="RM1" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="XLXN_30" name="I0" />
            <blockpin signalname="XLXN_24" name="I1" />
            <blockpin signalname="RM2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_15">
            <blockpin signalname="XLXN_25" name="I0" />
            <blockpin signalname="XLXN_26" name="I1" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="I0" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="I0" name="I" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="LM2">
            <wire x2="2288" y1="816" y2="816" x1="1888" />
        </branch>
        <iomarker fontsize="28" x="2288" y="816" name="LM2" orien="R0" />
        <instance x="1632" y="912" name="XLXI_3" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1632" y1="848" y2="848" x1="1600" />
        </branch>
        <instance x="1344" y="944" name="XLXI_4" orien="R0" />
        <branch name="RM1">
            <wire x2="1248" y1="336" y2="736" x1="1248" />
            <wire x2="1328" y1="736" y2="736" x1="1248" />
            <wire x2="1328" y1="736" y2="816" x1="1328" />
            <wire x2="1344" y1="816" y2="816" x1="1328" />
            <wire x2="1328" y1="816" y2="1152" x1="1328" />
            <wire x2="1904" y1="1152" y2="1152" x1="1328" />
            <wire x2="1728" y1="336" y2="336" x1="1248" />
            <wire x2="1328" y1="816" y2="816" x1="1312" />
        </branch>
        <instance x="1088" y="848" name="XLXI_6" orien="R0" />
        <instance x="1344" y="752" name="XLXI_2" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="1600" y1="720" y2="720" x1="1568" />
            <wire x2="1600" y1="720" y2="784" x1="1600" />
            <wire x2="1632" y1="784" y2="784" x1="1600" />
        </branch>
        <instance x="1728" y="592" name="XLXI_7" orien="R0" />
        <branch name="XLXN_13">
            <wire x2="2016" y1="496" y2="496" x1="1984" />
        </branch>
        <instance x="2016" y="560" name="XLXI_8" orien="R0" />
        <branch name="XLXN_14">
            <wire x2="2304" y1="464" y2="464" x1="2272" />
        </branch>
        <instance x="2304" y="528" name="XLXI_9" orien="R0" />
        <branch name="I2">
            <wire x2="928" y1="592" y2="592" x1="592" />
            <wire x2="928" y1="592" y2="880" x1="928" />
            <wire x2="1040" y1="880" y2="880" x1="928" />
            <wire x2="896" y1="400" y2="880" x1="896" />
            <wire x2="928" y1="880" y2="880" x1="896" />
            <wire x2="1728" y1="400" y2="400" x1="896" />
            <wire x2="1728" y1="528" y2="528" x1="928" />
            <wire x2="928" y1="528" y2="592" x1="928" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="2288" y1="288" y2="288" x1="2272" />
            <wire x2="2288" y1="288" y2="400" x1="2288" />
            <wire x2="2304" y1="400" y2="400" x1="2288" />
        </branch>
        <instance x="2016" y="384" name="XLXI_11" orien="R0" />
        <branch name="XLXN_19">
            <wire x2="2016" y1="256" y2="256" x1="1984" />
        </branch>
        <instance x="1760" y="288" name="XLXI_12" orien="R0" />
        <instance x="1728" y="464" name="XLXI_13" orien="R0" />
        <branch name="XLXN_21">
            <wire x2="2000" y1="368" y2="368" x1="1984" />
            <wire x2="2016" y1="320" y2="320" x1="2000" />
            <wire x2="2000" y1="320" y2="368" x1="2000" />
        </branch>
        <branch name="LM1">
            <wire x2="2592" y1="432" y2="432" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2592" y="432" name="LM1" orien="R0" />
        <instance x="1040" y="912" name="XLXI_5" orien="R0" />
        <instance x="1616" y="1424" name="XLXI_14" orien="R0" />
        <branch name="XLXN_24">
            <wire x2="1296" y1="880" y2="880" x1="1264" />
            <wire x2="1344" y1="880" y2="880" x1="1296" />
            <wire x2="1296" y1="880" y2="1296" x1="1296" />
            <wire x2="1616" y1="1296" y2="1296" x1="1296" />
        </branch>
        <instance x="1328" y="1472" name="XLXI_15" orien="R0" />
        <branch name="XLXN_25">
            <wire x2="1328" y1="1408" y2="1408" x1="1296" />
        </branch>
        <instance x="1040" y="1504" name="XLXI_16" orien="R0" />
        <instance x="960" y="1328" name="XLXI_17" orien="R0" />
        <branch name="XLXN_26">
            <wire x2="1248" y1="1296" y2="1296" x1="1184" />
            <wire x2="1248" y1="1296" y2="1344" x1="1248" />
            <wire x2="1312" y1="1344" y2="1344" x1="1248" />
            <wire x2="1328" y1="1344" y2="1344" x1="1312" />
        </branch>
        <branch name="I0">
            <wire x2="816" y1="368" y2="368" x1="592" />
            <wire x2="1328" y1="368" y2="368" x1="816" />
            <wire x2="1328" y1="368" y2="720" x1="1328" />
            <wire x2="1344" y1="720" y2="720" x1="1328" />
            <wire x2="1664" y1="368" y2="368" x1="1328" />
            <wire x2="1664" y1="368" y2="432" x1="1664" />
            <wire x2="2016" y1="432" y2="432" x1="1664" />
            <wire x2="816" y1="368" y2="1296" x1="816" />
            <wire x2="960" y1="1296" y2="1296" x1="816" />
            <wire x2="816" y1="1296" y2="1376" x1="816" />
            <wire x2="1040" y1="1376" y2="1376" x1="816" />
            <wire x2="1760" y1="256" y2="256" x1="1328" />
            <wire x2="1328" y1="256" y2="368" x1="1328" />
        </branch>
        <branch name="I1">
            <wire x2="704" y1="464" y2="464" x1="592" />
            <wire x2="1072" y1="464" y2="464" x1="704" />
            <wire x2="1728" y1="464" y2="464" x1="1072" />
            <wire x2="1072" y1="464" y2="816" x1="1072" />
            <wire x2="1088" y1="816" y2="816" x1="1072" />
            <wire x2="704" y1="464" y2="1440" x1="704" />
            <wire x2="1040" y1="1440" y2="1440" x1="704" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="1600" y1="1376" y2="1376" x1="1584" />
            <wire x2="1600" y1="1360" y2="1376" x1="1600" />
            <wire x2="1616" y1="1360" y2="1360" x1="1600" />
        </branch>
        <branch name="RM2">
            <wire x2="1904" y1="1328" y2="1328" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="1904" y="1328" name="RM2" orien="R0" />
        <iomarker fontsize="28" x="592" y="368" name="I0" orien="R180" />
        <iomarker fontsize="28" x="592" y="464" name="I1" orien="R180" />
        <iomarker fontsize="28" x="592" y="592" name="I2" orien="R180" />
        <iomarker fontsize="28" x="1904" y="1152" name="RM1" orien="R0" />
    </sheet>
</drawing>