<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="D0" />
        <signal name="D1" />
        <signal name="D2" />
        <signal name="D3" />
        <signal name="CLK" />
        <signal name="CLR" />
        <signal name="Q4" />
        <signal name="Q7" />
        <signal name="Q6" />
        <signal name="Q5" />
        <signal name="D4" />
        <signal name="D5" />
        <signal name="D6" />
        <signal name="D7" />
        <signal name="D10" />
        <signal name="D11" />
        <signal name="D8" />
        <signal name="Q8" />
        <signal name="Q9" />
        <signal name="Q10" />
        <signal name="Q11" />
        <signal name="XLXN_256" />
        <signal name="XLXN_257" />
        <signal name="D9" />
        <signal name="XLXN_263" />
        <signal name="Q12" />
        <signal name="XLXN_272" />
        <signal name="D16" />
        <signal name="D15" />
        <signal name="D13" />
        <signal name="D14" />
        <signal name="D12" />
        <signal name="XLXN_278" />
        <signal name="XLXN_279" />
        <signal name="XLXN_280" />
        <signal name="XLXN_281" />
        <signal name="D17" />
        <signal name="XLXN_283" />
        <signal name="XLXN_285" />
        <signal name="XLXN_286" />
        <signal name="Q13" />
        <signal name="XLXN_288" />
        <signal name="XLXN_290" />
        <signal name="XLXN_291" />
        <signal name="XLXN_292" />
        <signal name="Q3" />
        <signal name="Q1" />
        <signal name="Q0" />
        <signal name="Q2" />
        <port polarity="Input" name="D0" />
        <port polarity="Input" name="D1" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D3" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="Q4" />
        <port polarity="Output" name="Q7" />
        <port polarity="Output" name="Q6" />
        <port polarity="Output" name="Q5" />
        <port polarity="Input" name="D4" />
        <port polarity="Input" name="D5" />
        <port polarity="Input" name="D6" />
        <port polarity="Input" name="D7" />
        <port polarity="Input" name="D10" />
        <port polarity="Input" name="D11" />
        <port polarity="Input" name="D8" />
        <port polarity="Output" name="Q8" />
        <port polarity="Output" name="Q9" />
        <port polarity="Output" name="Q10" />
        <port polarity="Output" name="Q11" />
        <port polarity="Input" name="D9" />
        <port polarity="Output" name="Q12" />
        <port polarity="Input" name="D16" />
        <port polarity="Input" name="D15" />
        <port polarity="Input" name="D13" />
        <port polarity="Input" name="D14" />
        <port polarity="Input" name="D12" />
        <port polarity="Input" name="D17" />
        <port polarity="Output" name="Q13" />
        <port polarity="Output" name="Q3" />
        <port polarity="Output" name="Q1" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q2" />
        <blockdef name="fdc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="decoder">
            <timestamp>2013-5-1T12:12:6</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="sipo">
            <timestamp>2013-5-1T14:32:3</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
        </blockdef>
        <blockdef name="clk_dividor">
            <timestamp>2013-5-2T15:37:6</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="sensors">
            <timestamp>2013-5-3T8:38:2</timestamp>
            <rect width="256" x="64" y="-448" height="448" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="fdc" name="XLXI_2">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D1" name="D" />
            <blockpin signalname="XLXN_290" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_3">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D2" name="D" />
            <blockpin signalname="XLXN_291" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_4">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D3" name="D" />
            <blockpin signalname="XLXN_292" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_1">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D0" name="D" />
            <blockpin signalname="XLXN_288" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_18">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D5" name="D" />
            <blockpin signalname="Q5" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_19">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D6" name="D" />
            <blockpin signalname="Q6" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_20">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D7" name="D" />
            <blockpin signalname="Q7" name="Q" />
        </block>
        <block symbolname="fdc" name="XLXI_21">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="D4" name="D" />
            <blockpin signalname="Q4" name="Q" />
        </block>
        <block symbolname="and2" name="XLXI_33">
            <blockpin signalname="D9" name="I0" />
            <blockpin signalname="D8" name="I1" />
            <blockpin signalname="XLXN_285" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_34">
            <blockpin signalname="D11" name="I0" />
            <blockpin signalname="D10" name="I1" />
            <blockpin signalname="XLXN_286" name="O" />
        </block>
        <block symbolname="sipo" name="XLXI_43">
            <blockpin signalname="XLXN_263" name="CLOCK" />
            <blockpin signalname="Q0" name="Q0" />
            <blockpin signalname="Q1" name="Q1" />
            <blockpin signalname="Q2" name="Q2" />
            <blockpin signalname="D8" name="SERIAL_IN" />
        </block>
        <block symbolname="decoder" name="XLXI_41">
            <blockpin signalname="Q0" name="I0" />
            <blockpin signalname="Q1" name="I1" />
            <blockpin signalname="Q2" name="I2" />
            <blockpin signalname="Q8" name="LM1" />
            <blockpin signalname="Q9" name="LM2" />
            <blockpin signalname="Q10" name="RM1" />
            <blockpin signalname="Q11" name="RM2" />
        </block>
        <block symbolname="clk_dividor" name="XLXI_45">
            <blockpin signalname="XLXN_256" name="CLK_IN" />
            <blockpin signalname="XLXN_263" name="SLOW_CLK_OUT" />
        </block>
        <block symbolname="clk_dividor" name="XLXI_46">
            <blockpin signalname="XLXN_257" name="CLK_IN" />
            <blockpin signalname="XLXN_256" name="SLOW_CLK_OUT" />
        </block>
        <block symbolname="clk_dividor" name="XLXI_47">
            <blockpin signalname="CLK" name="CLK_IN" />
            <blockpin signalname="XLXN_257" name="SLOW_CLK_OUT" />
        </block>
        <block symbolname="sensors" name="XLXI_48">
            <blockpin signalname="XLXN_278" name="B0" />
            <blockpin signalname="XLXN_279" name="B1" />
            <blockpin signalname="XLXN_280" name="B2" />
            <blockpin signalname="XLXN_281" name="B3" />
            <blockpin signalname="XLXN_272" name="B4" />
            <blockpin signalname="XLXN_283" name="B5" />
            <blockpin signalname="XLXN_263" name="CLK" />
            <blockpin signalname="Q12" name="DATA_OUT" />
        </block>
        <block symbolname="inv" name="XLXI_51">
            <blockpin signalname="D13" name="I" />
            <blockpin signalname="XLXN_279" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_53">
            <blockpin signalname="D15" name="I" />
            <blockpin signalname="XLXN_281" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_54">
            <blockpin signalname="D16" name="I" />
            <blockpin signalname="XLXN_272" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_56">
            <blockpin signalname="D14" name="I" />
            <blockpin signalname="XLXN_280" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_57">
            <blockpin signalname="D12" name="I" />
            <blockpin signalname="XLXN_278" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_58">
            <blockpin signalname="D17" name="I" />
            <blockpin signalname="XLXN_283" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_59">
            <blockpin signalname="XLXN_286" name="I0" />
            <blockpin signalname="XLXN_285" name="I1" />
            <blockpin signalname="Q13" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_60">
            <blockpin signalname="XLXN_292" name="I0" />
            <blockpin signalname="XLXN_291" name="I1" />
            <blockpin signalname="XLXN_290" name="I2" />
            <blockpin signalname="XLXN_288" name="I3" />
            <blockpin signalname="Q3" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="720" y="800" name="XLXI_2" orien="R0" />
        <instance x="720" y="1152" name="XLXI_3" orien="R0" />
        <instance x="720" y="1504" name="XLXI_4" orien="R0" />
        <instance x="720" y="448" name="XLXI_1" orien="R0" />
        <branch name="D0">
            <wire x2="720" y1="192" y2="192" x1="288" />
        </branch>
        <branch name="D1">
            <wire x2="560" y1="240" y2="240" x1="288" />
            <wire x2="560" y1="240" y2="544" x1="560" />
            <wire x2="720" y1="544" y2="544" x1="560" />
        </branch>
        <branch name="D2">
            <wire x2="528" y1="288" y2="288" x1="288" />
            <wire x2="528" y1="288" y2="896" x1="528" />
            <wire x2="720" y1="896" y2="896" x1="528" />
        </branch>
        <branch name="D3">
            <wire x2="496" y1="336" y2="336" x1="288" />
            <wire x2="496" y1="336" y2="1248" x1="496" />
            <wire x2="720" y1="1248" y2="1248" x1="496" />
        </branch>
        <instance x="2336" y="800" name="XLXI_18" orien="R0" />
        <instance x="2336" y="1152" name="XLXI_19" orien="R0" />
        <instance x="2336" y="1504" name="XLXI_20" orien="R0" />
        <instance x="2336" y="448" name="XLXI_21" orien="R0" />
        <branch name="Q4">
            <wire x2="3216" y1="192" y2="192" x1="2720" />
        </branch>
        <branch name="Q7">
            <wire x2="2912" y1="1248" y2="1248" x1="2720" />
            <wire x2="2912" y1="336" y2="1248" x1="2912" />
            <wire x2="3216" y1="336" y2="336" x1="2912" />
        </branch>
        <branch name="Q6">
            <wire x2="2880" y1="896" y2="896" x1="2720" />
            <wire x2="2880" y1="288" y2="896" x1="2880" />
            <wire x2="3216" y1="288" y2="288" x1="2880" />
        </branch>
        <branch name="Q5">
            <wire x2="2848" y1="544" y2="544" x1="2720" />
            <wire x2="3216" y1="240" y2="240" x1="2848" />
            <wire x2="2848" y1="240" y2="544" x1="2848" />
        </branch>
        <branch name="D4">
            <wire x2="2336" y1="192" y2="192" x1="1904" />
        </branch>
        <branch name="D5">
            <wire x2="2176" y1="240" y2="240" x1="1904" />
            <wire x2="2176" y1="240" y2="544" x1="2176" />
            <wire x2="2336" y1="544" y2="544" x1="2176" />
        </branch>
        <branch name="D6">
            <wire x2="2144" y1="288" y2="288" x1="1904" />
            <wire x2="2144" y1="288" y2="896" x1="2144" />
            <wire x2="2336" y1="896" y2="896" x1="2144" />
        </branch>
        <branch name="D7">
            <wire x2="2112" y1="336" y2="336" x1="1904" />
            <wire x2="2112" y1="336" y2="1248" x1="2112" />
            <wire x2="2336" y1="1248" y2="1248" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="288" y="192" name="D0" orien="R180" />
        <iomarker fontsize="28" x="288" y="240" name="D1" orien="R180" />
        <iomarker fontsize="28" x="288" y="288" name="D2" orien="R180" />
        <iomarker fontsize="28" x="288" y="336" name="D3" orien="R180" />
        <iomarker fontsize="28" x="3216" y="192" name="Q4" orien="R0" />
        <iomarker fontsize="28" x="1904" y="192" name="D4" orien="R180" />
        <iomarker fontsize="28" x="1904" y="240" name="D5" orien="R180" />
        <iomarker fontsize="28" x="1904" y="288" name="D6" orien="R180" />
        <iomarker fontsize="28" x="1904" y="336" name="D7" orien="R180" />
        <iomarker fontsize="28" x="3216" y="240" name="Q5" orien="R0" />
        <iomarker fontsize="28" x="3216" y="288" name="Q6" orien="R0" />
        <iomarker fontsize="28" x="3216" y="336" name="Q7" orien="R0" />
        <branch name="CLK">
            <wire x2="608" y1="1568" y2="1568" x1="208" />
            <wire x2="944" y1="1568" y2="1568" x1="608" />
            <wire x2="2224" y1="1568" y2="1568" x1="944" />
            <wire x2="720" y1="320" y2="320" x1="608" />
            <wire x2="608" y1="320" y2="672" x1="608" />
            <wire x2="720" y1="672" y2="672" x1="608" />
            <wire x2="608" y1="672" y2="1024" x1="608" />
            <wire x2="720" y1="1024" y2="1024" x1="608" />
            <wire x2="608" y1="1024" y2="1376" x1="608" />
            <wire x2="720" y1="1376" y2="1376" x1="608" />
            <wire x2="608" y1="1376" y2="1568" x1="608" />
            <wire x2="944" y1="1552" y2="1568" x1="944" />
            <wire x2="2976" y1="1552" y2="1552" x1="944" />
            <wire x2="2976" y1="1552" y2="2512" x1="2976" />
            <wire x2="2336" y1="320" y2="320" x1="2224" />
            <wire x2="2224" y1="320" y2="672" x1="2224" />
            <wire x2="2336" y1="672" y2="672" x1="2224" />
            <wire x2="2224" y1="672" y2="1024" x1="2224" />
            <wire x2="2336" y1="1024" y2="1024" x1="2224" />
            <wire x2="2224" y1="1024" y2="1376" x1="2224" />
            <wire x2="2336" y1="1376" y2="1376" x1="2224" />
            <wire x2="2224" y1="1376" y2="1568" x1="2224" />
            <wire x2="2976" y1="2512" y2="2512" x1="2928" />
        </branch>
        <branch name="CLR">
            <wire x2="640" y1="1632" y2="1632" x1="208" />
            <wire x2="2256" y1="1632" y2="1632" x1="640" />
            <wire x2="720" y1="416" y2="416" x1="640" />
            <wire x2="640" y1="416" y2="768" x1="640" />
            <wire x2="720" y1="768" y2="768" x1="640" />
            <wire x2="640" y1="768" y2="1120" x1="640" />
            <wire x2="720" y1="1120" y2="1120" x1="640" />
            <wire x2="640" y1="1120" y2="1472" x1="640" />
            <wire x2="720" y1="1472" y2="1472" x1="640" />
            <wire x2="640" y1="1472" y2="1632" x1="640" />
            <wire x2="2336" y1="416" y2="416" x1="2256" />
            <wire x2="2256" y1="416" y2="768" x1="2256" />
            <wire x2="2336" y1="768" y2="768" x1="2256" />
            <wire x2="2256" y1="768" y2="1120" x1="2256" />
            <wire x2="2336" y1="1120" y2="1120" x1="2256" />
            <wire x2="2256" y1="1120" y2="1472" x1="2256" />
            <wire x2="2336" y1="1472" y2="1472" x1="2256" />
            <wire x2="2256" y1="1472" y2="1632" x1="2256" />
        </branch>
        <iomarker fontsize="28" x="208" y="1568" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="208" y="1632" name="CLR" orien="R180" />
        <branch name="D10">
            <wire x2="704" y1="1920" y2="1920" x1="272" />
        </branch>
        <iomarker fontsize="28" x="272" y="1920" name="D10" orien="R180" />
        <iomarker fontsize="28" x="256" y="1776" name="D8" orien="R180" />
        <instance x="672" y="1904" name="XLXI_33" orien="R0" />
        <branch name="D11">
            <wire x2="704" y1="1984" y2="1984" x1="272" />
        </branch>
        <iomarker fontsize="28" x="272" y="1984" name="D11" orien="R180" />
        <branch name="D8">
            <wire x2="416" y1="1776" y2="1776" x1="256" />
            <wire x2="672" y1="1776" y2="1776" x1="416" />
            <wire x2="416" y1="1680" y2="1776" x1="416" />
            <wire x2="1840" y1="1680" y2="1680" x1="416" />
            <wire x2="1840" y1="1680" y2="2112" x1="1840" />
            <wire x2="1936" y1="2112" y2="2112" x1="1840" />
        </branch>
        <instance x="1936" y="2272" name="XLXI_43" orien="R0">
        </instance>
        <instance x="2576" y="2336" name="XLXI_41" orien="R0">
        </instance>
        <branch name="Q8">
            <wire x2="2992" y1="2112" y2="2112" x1="2960" />
        </branch>
        <branch name="Q9">
            <wire x2="2992" y1="2176" y2="2176" x1="2960" />
        </branch>
        <branch name="Q10">
            <wire x2="2992" y1="2240" y2="2240" x1="2960" />
        </branch>
        <branch name="Q11">
            <wire x2="2992" y1="2304" y2="2304" x1="2960" />
        </branch>
        <iomarker fontsize="28" x="2992" y="2112" name="Q8" orien="R0" />
        <iomarker fontsize="28" x="2992" y="2176" name="Q9" orien="R0" />
        <iomarker fontsize="28" x="2992" y="2240" name="Q10" orien="R0" />
        <iomarker fontsize="28" x="2992" y="2304" name="Q11" orien="R0" />
        <iomarker fontsize="28" x="2336" y="1776" name="Q0" orien="R270" />
        <iomarker fontsize="28" x="2400" y="1808" name="Q1" orien="R270" />
        <iomarker fontsize="28" x="2464" y="1824" name="Q2" orien="R270" />
        <branch name="XLXN_256">
            <wire x2="2032" y1="2512" y2="2512" x1="1952" />
        </branch>
        <branch name="XLXN_257">
            <wire x2="2544" y1="2512" y2="2512" x1="2416" />
        </branch>
        <branch name="D9">
            <wire x2="672" y1="1840" y2="1840" x1="256" />
        </branch>
        <iomarker fontsize="28" x="256" y="1840" name="D9" orien="R180" />
        <instance x="2928" y="2480" name="XLXI_47" orien="R180">
        </instance>
        <instance x="2416" y="2480" name="XLXI_46" orien="R180">
        </instance>
        <instance x="1952" y="2480" name="XLXI_45" orien="R180">
        </instance>
        <instance x="704" y="2048" name="XLXI_34" orien="R0" />
        <instance x="816" y="2576" name="XLXI_48" orien="R0">
        </instance>
        <branch name="XLXN_263">
            <wire x2="656" y1="2032" y2="2160" x1="656" />
            <wire x2="816" y1="2160" y2="2160" x1="656" />
            <wire x2="1360" y1="2032" y2="2032" x1="656" />
            <wire x2="1360" y1="2032" y2="2512" x1="1360" />
            <wire x2="1536" y1="2512" y2="2512" x1="1360" />
            <wire x2="1568" y1="2512" y2="2512" x1="1536" />
            <wire x2="1536" y1="2176" y2="2512" x1="1536" />
            <wire x2="1936" y1="2176" y2="2176" x1="1536" />
        </branch>
        <branch name="Q12">
            <wire x2="1232" y1="2160" y2="2160" x1="1200" />
        </branch>
        <iomarker fontsize="28" x="1232" y="2160" name="Q12" orien="R0" />
        <iomarker fontsize="28" x="352" y="2224" name="D12" orien="R180" />
        <iomarker fontsize="28" x="352" y="2288" name="D13" orien="R180" />
        <iomarker fontsize="28" x="352" y="2352" name="D14" orien="R180" />
        <iomarker fontsize="28" x="352" y="2416" name="D15" orien="R180" />
        <iomarker fontsize="28" x="352" y="2480" name="D16" orien="R180" />
        <iomarker fontsize="28" x="352" y="2544" name="D17" orien="R180" />
        <instance x="464" y="2320" name="XLXI_51" orien="R0" />
        <branch name="XLXN_272">
            <wire x2="816" y1="2480" y2="2480" x1="784" />
        </branch>
        <instance x="560" y="2512" name="XLXI_54" orien="R0" />
        <branch name="D16">
            <wire x2="560" y1="2480" y2="2480" x1="352" />
        </branch>
        <instance x="512" y="2448" name="XLXI_53" orien="R0" />
        <branch name="D15">
            <wire x2="512" y1="2416" y2="2416" x1="352" />
        </branch>
        <instance x="512" y="2384" name="XLXI_56" orien="R0" />
        <branch name="D13">
            <wire x2="464" y1="2288" y2="2288" x1="352" />
        </branch>
        <branch name="D14">
            <wire x2="512" y1="2352" y2="2352" x1="352" />
        </branch>
        <instance x="544" y="2256" name="XLXI_57" orien="R0" />
        <branch name="D12">
            <wire x2="544" y1="2224" y2="2224" x1="352" />
        </branch>
        <branch name="XLXN_278">
            <wire x2="816" y1="2224" y2="2224" x1="768" />
        </branch>
        <branch name="XLXN_279">
            <wire x2="816" y1="2288" y2="2288" x1="688" />
        </branch>
        <branch name="XLXN_280">
            <wire x2="816" y1="2352" y2="2352" x1="736" />
        </branch>
        <branch name="XLXN_281">
            <wire x2="816" y1="2416" y2="2416" x1="736" />
        </branch>
        <instance x="560" y="2576" name="XLXI_58" orien="R0" />
        <branch name="D17">
            <wire x2="560" y1="2544" y2="2544" x1="352" />
        </branch>
        <branch name="XLXN_283">
            <wire x2="816" y1="2544" y2="2544" x1="784" />
        </branch>
        <instance x="1104" y="1920" name="XLXI_59" orien="R0" />
        <branch name="XLXN_285">
            <wire x2="1008" y1="1808" y2="1808" x1="928" />
            <wire x2="1008" y1="1792" y2="1808" x1="1008" />
            <wire x2="1104" y1="1792" y2="1792" x1="1008" />
        </branch>
        <branch name="XLXN_286">
            <wire x2="1024" y1="1952" y2="1952" x1="960" />
            <wire x2="1024" y1="1856" y2="1952" x1="1024" />
            <wire x2="1104" y1="1856" y2="1856" x1="1024" />
        </branch>
        <branch name="Q13">
            <wire x2="1392" y1="1824" y2="1824" x1="1360" />
        </branch>
        <iomarker fontsize="28" x="1392" y="1824" name="Q13" orien="R0" />
        <instance x="1392" y="1088" name="XLXI_60" orien="R0" />
        <branch name="XLXN_288">
            <wire x2="1392" y1="192" y2="192" x1="1104" />
            <wire x2="1392" y1="192" y2="832" x1="1392" />
        </branch>
        <branch name="XLXN_290">
            <wire x2="1328" y1="544" y2="544" x1="1104" />
            <wire x2="1328" y1="544" y2="896" x1="1328" />
            <wire x2="1392" y1="896" y2="896" x1="1328" />
        </branch>
        <branch name="XLXN_291">
            <wire x2="1248" y1="896" y2="896" x1="1104" />
            <wire x2="1248" y1="896" y2="960" x1="1248" />
            <wire x2="1392" y1="960" y2="960" x1="1248" />
        </branch>
        <branch name="XLXN_292">
            <wire x2="1392" y1="1248" y2="1248" x1="1104" />
            <wire x2="1392" y1="1024" y2="1248" x1="1392" />
        </branch>
        <branch name="Q3">
            <wire x2="1680" y1="928" y2="928" x1="1648" />
        </branch>
        <iomarker fontsize="28" x="1680" y="928" name="Q3" orien="R0" />
        <branch name="Q1">
            <wire x2="2400" y1="2112" y2="2112" x1="2320" />
            <wire x2="2432" y1="2112" y2="2112" x1="2400" />
            <wire x2="2432" y1="2112" y2="2176" x1="2432" />
            <wire x2="2576" y1="2176" y2="2176" x1="2432" />
            <wire x2="2400" y1="1808" y2="2112" x1="2400" />
        </branch>
        <branch name="Q0">
            <wire x2="2336" y1="2176" y2="2176" x1="2320" />
            <wire x2="2384" y1="2176" y2="2176" x1="2336" />
            <wire x2="2384" y1="2176" y2="2240" x1="2384" />
            <wire x2="2496" y1="2240" y2="2240" x1="2384" />
            <wire x2="2336" y1="1776" y2="2176" x1="2336" />
            <wire x2="2496" y1="2112" y2="2240" x1="2496" />
            <wire x2="2576" y1="2112" y2="2112" x1="2496" />
        </branch>
        <branch name="Q2">
            <wire x2="2368" y1="2240" y2="2240" x1="2320" />
            <wire x2="2368" y1="2240" y2="2304" x1="2368" />
            <wire x2="2464" y1="2304" y2="2304" x1="2368" />
            <wire x2="2560" y1="2304" y2="2304" x1="2464" />
            <wire x2="2464" y1="1824" y2="2304" x1="2464" />
            <wire x2="2560" y1="2240" y2="2304" x1="2560" />
            <wire x2="2576" y1="2240" y2="2240" x1="2560" />
        </branch>
    </sheet>
</drawing>