--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : Robot_V1.vhf
-- /___/   /\     Timestamp : 05/10/2013 13:11:40
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Robot/Robot_V1.vhf -w H:/Robot/Robot/Robot_V1.sch
--Design Name: Robot_V1
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD_MXILINX_Robot_V1 is
   generic( INIT : bit :=  '0');
   port ( C : in    std_logic; 
          D : in    std_logic; 
          Q : out   std_logic);
end FD_MXILINX_Robot_V1;

architecture BEHAVIORAL of FD_MXILINX_Robot_V1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_4 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_43 : GND
      port map (G=>XLXN_4);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>XLXN_4,
                D=>D,
                PRE=>XLXN_4,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity XOR6_MXILINX_Robot_V1 is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          O  : out   std_logic);
end XOR6_MXILINX_Robot_V1;

architecture BEHAVIORAL of XOR6_MXILINX_Robot_V1 is
   attribute BOX_TYPE   : string ;
   signal I35 : std_logic;
   component XOR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR4 : component is "BLACK_BOX";
   
   component XOR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR3 : component is "BLACK_BOX";
   
begin
   I_36_87 : XOR4
      port map (I0=>I0,
                I1=>I1,
                I2=>I2,
                I3=>I35,
                O=>O);
   
   I_36_88 : XOR3
      port map (I0=>I3,
                I1=>I4,
                I2=>I5,
                O=>I35);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1_MXILINX_Robot_V1 is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1_MXILINX_Robot_V1;

architecture BEHAVIORAL of M2_1_MXILINX_Robot_V1 is
   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
begin
   I_36_7 : AND2B1
      port map (I0=>S0,
                I1=>D0,
                O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1,
                I1=>S0,
                O=>M1);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity sensors_MUSER_Robot_V1 is
   port ( B0       : in    std_logic; 
          B1       : in    std_logic; 
          B2       : in    std_logic; 
          B3       : in    std_logic; 
          B4       : in    std_logic; 
          B5       : in    std_logic; 
          CLK      : in    std_logic; 
          DATA_OUT : out   std_logic);
end sensors_MUSER_Robot_V1;

architecture BEHAVIORAL of sensors_MUSER_Robot_V1 is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_14  : std_logic;
   signal XLXN_28  : std_logic;
   signal XLXN_29  : std_logic;
   signal XLXN_30  : std_logic;
   signal XLXN_31  : std_logic;
   signal XLXN_33  : std_logic;
   signal XLXN_34  : std_logic;
   signal XLXN_35  : std_logic;
   signal XLXN_36  : std_logic;
   signal XLXN_47  : std_logic;
   signal XLXN_48  : std_logic;
   signal XLXN_49  : std_logic;
   signal XLXN_50  : std_logic;
   signal XLXN_51  : std_logic;
   signal XLXN_54  : std_logic;
   signal XLXN_95  : std_logic;
   signal XLXN_97  : std_logic;
   signal XLXN_98  : std_logic;
   signal XLXN_99  : std_logic;
   signal XLXN_100 : std_logic;
   signal XLXN_101 : std_logic;
   signal XLXN_104 : std_logic;
   signal XLXN_106 : std_logic;
   signal XLXN_108 : std_logic;
   signal XLXN_110 : std_logic;
   signal XLXN_112 : std_logic;
   signal XLXN_113 : std_logic;
   signal XLXN_115 : std_logic;
   signal XLXN_129 : std_logic;
   signal XLXN_138 : std_logic;
   signal XLXN_139 : std_logic;
   signal XLXN_141 : std_logic;
   signal XLXN_142 : std_logic;
   signal XLXN_143 : std_logic;
   signal XLXN_144 : std_logic;
   signal XLXN_145 : std_logic;
   signal XLXN_146 : std_logic;
   signal XLXN_147 : std_logic;
   signal XLXN_149 : std_logic;
   signal XLXN_157 : std_logic;
   signal XLXN_158 : std_logic;
   signal XLXN_159 : std_logic;
   signal XLXN_160 : std_logic;
   signal XLXN_161 : std_logic;
   signal XLXN_178 : std_logic;
   signal XLXN_179 : std_logic;
   component FD_MXILINX_Robot_V1
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component M2_1_MXILINX_Robot_V1
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component XOR6_MXILINX_Robot_V1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component NAND4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND4 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_9 : label is "XLXI_9_53";
   attribute HU_SET of XLXI_10 : label is "XLXI_10_52";
   attribute HU_SET of XLXI_11 : label is "XLXI_11_51";
   attribute HU_SET of XLXI_12 : label is "XLXI_12_50";
   attribute HU_SET of XLXI_13 : label is "XLXI_13_49";
   attribute HU_SET of XLXI_14 : label is "XLXI_14_48";
   attribute HU_SET of XLXI_15 : label is "XLXI_15_47";
   attribute HU_SET of XLXI_16 : label is "XLXI_16_46";
   attribute HU_SET of XLXI_17 : label is "XLXI_17_59";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_58";
   attribute HU_SET of XLXI_19 : label is "XLXI_19_57";
   attribute HU_SET of XLXI_20 : label is "XLXI_20_56";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_55";
   attribute HU_SET of XLXI_22 : label is "XLXI_22_54";
   attribute HU_SET of XLXI_23 : label is "XLXI_23_60";
   attribute HU_SET of XLXI_40 : label is "XLXI_40_61";
   attribute HU_SET of XLXI_41 : label is "XLXI_41_62";
   attribute HU_SET of XLXI_42 : label is "XLXI_42_63";
   attribute HU_SET of XLXI_43 : label is "XLXI_43_64";
   attribute HU_SET of XLXI_44 : label is "XLXI_44_65";
   attribute HU_SET of XLXI_45 : label is "XLXI_45_66";
   attribute HU_SET of XLXI_46 : label is "XLXI_46_67";
   attribute HU_SET of XLXI_47 : label is "XLXI_47_68";
   attribute HU_SET of XLXI_48 : label is "XLXI_48_69";
   attribute HU_SET of XLXI_49 : label is "XLXI_49_70";
   attribute HU_SET of XLXI_50 : label is "XLXI_50_71";
   attribute HU_SET of XLXI_51 : label is "XLXI_51_72";
   attribute HU_SET of XLXI_52 : label is "XLXI_52_73";
   attribute HU_SET of XLXI_53 : label is "XLXI_53_74";
   attribute HU_SET of XLXI_54 : label is "XLXI_54_75";
   attribute HU_SET of XLXI_56 : label is "XLXI_56_76";
   attribute HU_SET of XLXI_63 : label is "XLXI_63_77";
   attribute HU_SET of XLXI_64 : label is "XLXI_64_78";
   attribute HU_SET of XLXI_65 : label is "XLXI_65_79";
   attribute HU_SET of XLXI_66 : label is "XLXI_66_80";
begin
   XLXI_9 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_145,
                Q=>XLXN_28);
   
   XLXI_10 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_14,
                Q=>XLXN_29);
   
   XLXI_11 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_47,
                Q=>XLXN_30);
   
   XLXI_12 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_48,
                Q=>XLXN_31);
   
   XLXI_13 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_49,
                Q=>XLXN_33);
   
   XLXI_14 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_50,
                Q=>XLXN_34);
   
   XLXI_15 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_51,
                Q=>XLXN_35);
   
   XLXI_16 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_54,
                Q=>XLXN_36);
   
   XLXI_17 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_145,
                D1=>XLXN_28,
                S0=>XLXN_179,
                O=>XLXN_14);
   
   XLXI_18 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_141,
                D1=>XLXN_29,
                S0=>XLXN_179,
                O=>XLXN_47);
   
   XLXI_19 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_142,
                D1=>XLXN_30,
                S0=>XLXN_179,
                O=>XLXN_48);
   
   XLXI_20 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_143,
                D1=>XLXN_31,
                S0=>XLXN_179,
                O=>XLXN_49);
   
   XLXI_21 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_144,
                D1=>XLXN_33,
                S0=>XLXN_179,
                O=>XLXN_50);
   
   XLXI_22 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_139,
                D1=>XLXN_34,
                S0=>XLXN_179,
                O=>XLXN_51);
   
   XLXI_23 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_138,
                D1=>XLXN_35,
                S0=>XLXN_179,
                O=>XLXN_54);
   
   XLXI_40 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_115,
                Q=>DATA_OUT);
   
   XLXI_41 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_112,
                Q=>XLXN_113);
   
   XLXI_42 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_110,
                Q=>XLXN_97);
   
   XLXI_43 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_108,
                Q=>XLXN_98);
   
   XLXI_44 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_106,
                Q=>XLXN_99);
   
   XLXI_45 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_104,
                Q=>XLXN_100);
   
   XLXI_46 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_95,
                Q=>XLXN_101);
   
   XLXI_47 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_36,
                Q=>XLXN_178);
   
   XLXI_48 : M2_1_MXILINX_Robot_V1
      port map (D0=>B5,
                D1=>XLXN_97,
                S0=>XLXN_179,
                O=>XLXN_112);
   
   XLXI_49 : M2_1_MXILINX_Robot_V1
      port map (D0=>B4,
                D1=>XLXN_98,
                S0=>XLXN_179,
                O=>XLXN_110);
   
   XLXI_50 : M2_1_MXILINX_Robot_V1
      port map (D0=>B3,
                D1=>XLXN_99,
                S0=>XLXN_179,
                O=>XLXN_108);
   
   XLXI_51 : M2_1_MXILINX_Robot_V1
      port map (D0=>B2,
                D1=>XLXN_100,
                S0=>XLXN_179,
                O=>XLXN_106);
   
   XLXI_52 : M2_1_MXILINX_Robot_V1
      port map (D0=>B1,
                D1=>XLXN_101,
                S0=>XLXN_179,
                O=>XLXN_104);
   
   XLXI_53 : M2_1_MXILINX_Robot_V1
      port map (D0=>B0,
                D1=>XLXN_178,
                S0=>XLXN_179,
                O=>XLXN_95);
   
   XLXI_54 : M2_1_MXILINX_Robot_V1
      port map (D0=>XLXN_129,
                D1=>XLXN_113,
                S0=>XLXN_179,
                O=>XLXN_115);
   
   XLXI_55 : GND
      port map (G=>XLXN_129);
   
   XLXI_56 : XOR6_MXILINX_Robot_V1
      port map (I0=>B5,
                I1=>B4,
                I2=>B3,
                I3=>B2,
                I4=>B1,
                I5=>B0,
                O=>XLXN_138);
   
   XLXI_57 : VCC
      port map (P=>XLXN_139);
   
   XLXI_58 : VCC
      port map (P=>XLXN_144);
   
   XLXI_59 : VCC
      port map (P=>XLXN_143);
   
   XLXI_60 : VCC
      port map (P=>XLXN_142);
   
   XLXI_61 : VCC
      port map (P=>XLXN_141);
   
   XLXI_62 : VCC
      port map (P=>XLXN_145);
   
   XLXI_63 : FD_MXILINX_Robot_V1
      port map (C=>CLK,
                D=>XLXN_147,
                Q=>XLXN_146);
   
   XLXI_64 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_146,
                D=>XLXN_157,
                Q=>XLXN_149);
   
   XLXI_65 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_149,
                D=>XLXN_159,
                Q=>XLXN_158);
   
   XLXI_66 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_158,
                D=>XLXN_160,
                Q=>XLXN_161);
   
   XLXI_68 : INV
      port map (I=>XLXN_146,
                O=>XLXN_147);
   
   XLXI_69 : INV
      port map (I=>XLXN_149,
                O=>XLXN_157);
   
   XLXI_70 : INV
      port map (I=>XLXN_158,
                O=>XLXN_159);
   
   XLXI_71 : INV
      port map (I=>XLXN_161,
                O=>XLXN_160);
   
   XLXI_74 : NAND4
      port map (I0=>XLXN_146,
                I1=>XLXN_149,
                I2=>XLXN_158,
                I3=>XLXN_161,
                O=>XLXN_179);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity clk_dividor_MUSER_Robot_V1 is
   port ( CLK_IN       : in    std_logic; 
          SLOW_CLK_OUT : out   std_logic);
end clk_dividor_MUSER_Robot_V1;

architecture BEHAVIORAL of clk_dividor_MUSER_Robot_V1 is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_1             : std_logic;
   signal XLXN_2             : std_logic;
   signal XLXN_4             : std_logic;
   signal XLXN_5             : std_logic;
   signal XLXN_7             : std_logic;
   signal XLXN_9             : std_logic;
   signal XLXN_11            : std_logic;
   signal SLOW_CLK_OUT_DUMMY : std_logic;
   component FD_MXILINX_Robot_V1
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_81";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_82";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_83";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_84";
begin
   SLOW_CLK_OUT <= SLOW_CLK_OUT_DUMMY;
   XLXI_1 : FD_MXILINX_Robot_V1
      port map (C=>CLK_IN,
                D=>XLXN_2,
                Q=>XLXN_1);
   
   XLXI_2 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_2,
                D=>XLXN_4,
                Q=>XLXN_5);
   
   XLXI_3 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_4,
                D=>XLXN_11,
                Q=>XLXN_7);
   
   XLXI_4 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_11,
                D=>SLOW_CLK_OUT_DUMMY,
                Q=>XLXN_9);
   
   XLXI_5 : INV
      port map (I=>XLXN_1,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>XLXN_5,
                O=>XLXN_4);
   
   XLXI_7 : INV
      port map (I=>XLXN_7,
                O=>XLXN_11);
   
   XLXI_8 : INV
      port map (I=>XLXN_9,
                O=>SLOW_CLK_OUT_DUMMY);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity decoder_MUSER_Robot_V1 is
   port ( I0  : in    std_logic; 
          I1  : in    std_logic; 
          I2  : in    std_logic; 
          LM1 : out   std_logic; 
          LM2 : out   std_logic; 
          RM1 : out   std_logic; 
          RM2 : out   std_logic);
end decoder_MUSER_Robot_V1;

architecture BEHAVIORAL of decoder_MUSER_Robot_V1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_5    : std_logic;
   signal XLXN_9    : std_logic;
   signal XLXN_13   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_18   : std_logic;
   signal XLXN_19   : std_logic;
   signal XLXN_21   : std_logic;
   signal XLXN_24   : std_logic;
   signal XLXN_25   : std_logic;
   signal XLXN_26   : std_logic;
   signal XLXN_30   : std_logic;
   signal RM1_DUMMY : std_logic;
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   RM1 <= RM1_DUMMY;
   XLXI_2 : INV
      port map (I=>I0,
                O=>XLXN_9);
   
   XLXI_3 : AND2
      port map (I0=>XLXN_5,
                I1=>XLXN_9,
                O=>LM2);
   
   XLXI_4 : OR2
      port map (I0=>XLXN_24,
                I1=>RM1_DUMMY,
                O=>XLXN_5);
   
   XLXI_5 : INV
      port map (I=>I2,
                O=>XLXN_24);
   
   XLXI_6 : INV
      port map (I=>I1,
                O=>RM1_DUMMY);
   
   XLXI_7 : XOR2
      port map (I0=>I2,
                I1=>I1,
                O=>XLXN_13);
   
   XLXI_8 : AND2
      port map (I0=>XLXN_13,
                I1=>I0,
                O=>XLXN_14);
   
   XLXI_9 : OR2
      port map (I0=>XLXN_14,
                I1=>XLXN_18,
                O=>LM1);
   
   XLXI_11 : AND2
      port map (I0=>XLXN_21,
                I1=>XLXN_19,
                O=>XLXN_18);
   
   XLXI_12 : INV
      port map (I=>I0,
                O=>XLXN_19);
   
   XLXI_13 : XOR2
      port map (I0=>I2,
                I1=>RM1_DUMMY,
                O=>XLXN_21);
   
   XLXI_14 : AND2
      port map (I0=>XLXN_30,
                I1=>XLXN_24,
                O=>RM2);
   
   XLXI_15 : OR2
      port map (I0=>XLXN_25,
                I1=>XLXN_26,
                O=>XLXN_30);
   
   XLXI_16 : AND2
      port map (I0=>I1,
                I1=>I0,
                O=>XLXN_25);
   
   XLXI_17 : INV
      port map (I=>I0,
                O=>XLXN_26);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity sipo_MUSER_Robot_V1 is
   port ( CLOCK     : in    std_logic; 
          SERIAL_IN : in    std_logic; 
          Q0        : out   std_logic; 
          Q1        : out   std_logic; 
          Q2        : out   std_logic);
end sipo_MUSER_Robot_V1;

architecture BEHAVIORAL of sipo_MUSER_Robot_V1 is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_139  : std_logic;
   signal XLXN_192  : std_logic;
   signal XLXN_194  : std_logic;
   signal XLXN_199  : std_logic;
   signal XLXN_201  : std_logic;
   signal XLXN_202  : std_logic;
   signal XLXN_205  : std_logic;
   signal XLXN_209  : std_logic;
   signal XLXN_211  : std_logic;
   signal XLXN_212  : std_logic;
   signal XLXN_213  : std_logic;
   signal XLXN_214  : std_logic;
   signal XLXN_215  : std_logic;
   component FD_MXILINX_Robot_V1
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component XOR3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR3 : component is "BLACK_BOX";
   
   component XNOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XNOR2 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_2 : label is "XLXI_2_85";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_86";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_87";
   attribute HU_SET of XLXI_5 : label is "XLXI_5_88";
   attribute HU_SET of XLXI_35 : label is "XLXI_35_89";
   attribute HU_SET of XLXI_36 : label is "XLXI_36_90";
   attribute HU_SET of XLXI_37 : label is "XLXI_37_91";
   attribute HU_SET of XLXI_38 : label is "XLXI_38_92";
   attribute HU_SET of XLXI_40 : label is "XLXI_40_93";
   attribute HU_SET of XLXI_41 : label is "XLXI_41_94";
   attribute HU_SET of XLXI_42 : label is "XLXI_42_95";
begin
   XLXI_2 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>SERIAL_IN,
                Q=>XLXN_199);
   
   XLXI_3 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_199,
                Q=>XLXN_201);
   
   XLXI_4 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_201,
                Q=>XLXN_202);
   
   XLXI_5 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_202,
                Q=>XLXN_214);
   
   XLXI_35 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_214,
                Q=>XLXN_139);
   
   XLXI_36 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_139,
                Q=>XLXN_213);
   
   XLXI_37 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_213,
                Q=>XLXN_215);
   
   XLXI_38 : FD_MXILINX_Robot_V1
      port map (C=>CLOCK,
                D=>XLXN_215,
                Q=>XLXN_205);
   
   XLXI_40 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_192,
                D=>XLXN_215,
                Q=>Q2);
   
   XLXI_41 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_192,
                D=>XLXN_213,
                Q=>Q1);
   
   XLXI_42 : FD_MXILINX_Robot_V1
      port map (C=>XLXN_192,
                D=>XLXN_139,
                Q=>Q0);
   
   XLXI_60 : AND3
      port map (I0=>XLXN_199,
                I1=>XLXN_201,
                I2=>XLXN_202,
                O=>XLXN_194);
   
   XLXI_61 : AND3
      port map (I0=>XLXN_194,
                I1=>XLXN_209,
                I2=>XLXN_211,
                O=>XLXN_192);
   
   XLXI_62 : INV
      port map (I=>XLXN_205,
                O=>XLXN_209);
   
   XLXI_64 : XOR3
      port map (I0=>XLXN_215,
                I1=>XLXN_213,
                I2=>XLXN_139,
                O=>XLXN_212);
   
   XLXI_80 : XNOR2
      port map (I0=>XLXN_214,
                I1=>XLXN_212,
                O=>XLXN_211);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FDC_MXILINX_Robot_V1 is
   generic( INIT : bit :=  '0');
   port ( C   : in    std_logic; 
          CLR : in    std_logic; 
          D   : in    std_logic; 
          Q   : out   std_logic);
end FDC_MXILINX_Robot_V1;

architecture BEHAVIORAL of FDC_MXILINX_Robot_V1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_5 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_55 : GND
      port map (G=>XLXN_5);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>CLR,
                D=>D,
                PRE=>XLXN_5,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Robot_V1 is
   port ( CLK : in    std_logic; 
          CLR : in    std_logic; 
          D0  : in    std_logic; 
          D1  : in    std_logic; 
          D2  : in    std_logic; 
          D3  : in    std_logic; 
          D4  : in    std_logic; 
          D5  : in    std_logic; 
          D6  : in    std_logic; 
          D7  : in    std_logic; 
          D8  : in    std_logic; 
          D9  : in    std_logic; 
          D10 : in    std_logic; 
          D11 : in    std_logic; 
          D12 : in    std_logic; 
          D13 : in    std_logic; 
          D14 : in    std_logic; 
          D15 : in    std_logic; 
          D16 : in    std_logic; 
          D17 : in    std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          Q2  : out   std_logic; 
          Q3  : out   std_logic; 
          Q4  : out   std_logic; 
          Q5  : out   std_logic; 
          Q6  : out   std_logic; 
          Q7  : out   std_logic; 
          Q8  : out   std_logic; 
          Q9  : out   std_logic; 
          Q10 : out   std_logic; 
          Q11 : out   std_logic; 
          Q12 : out   std_logic; 
          Q13 : out   std_logic);
end Robot_V1;

architecture BEHAVIORAL of Robot_V1 is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_256 : std_logic;
   signal XLXN_257 : std_logic;
   signal XLXN_263 : std_logic;
   signal XLXN_272 : std_logic;
   signal XLXN_278 : std_logic;
   signal XLXN_279 : std_logic;
   signal XLXN_280 : std_logic;
   signal XLXN_281 : std_logic;
   signal XLXN_283 : std_logic;
   signal XLXN_285 : std_logic;
   signal XLXN_286 : std_logic;
   signal XLXN_288 : std_logic;
   signal XLXN_290 : std_logic;
   signal XLXN_291 : std_logic;
   signal XLXN_292 : std_logic;
   signal Q0_DUMMY : std_logic;
   signal Q1_DUMMY : std_logic;
   signal Q2_DUMMY : std_logic;
   component FDC_MXILINX_Robot_V1
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component NAND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND2 : component is "BLACK_BOX";
   
   component decoder_MUSER_Robot_V1
      port ( I0  : in    std_logic; 
             I1  : in    std_logic; 
             I2  : in    std_logic; 
             LM1 : out   std_logic; 
             LM2 : out   std_logic; 
             RM1 : out   std_logic; 
             RM2 : out   std_logic);
   end component;
   
   component sipo_MUSER_Robot_V1
      port ( CLOCK     : in    std_logic; 
             Q0        : out   std_logic; 
             Q1        : out   std_logic; 
             Q2        : out   std_logic; 
             SERIAL_IN : in    std_logic);
   end component;
   
   component clk_dividor_MUSER_Robot_V1
      port ( CLK_IN       : in    std_logic; 
             SLOW_CLK_OUT : out   std_logic);
   end component;
   
   component sensors_MUSER_Robot_V1
      port ( B0       : in    std_logic; 
             B1       : in    std_logic; 
             B2       : in    std_logic; 
             B3       : in    std_logic; 
             B4       : in    std_logic; 
             B5       : in    std_logic; 
             CLK      : in    std_logic; 
             DATA_OUT : out   std_logic);
   end component;
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_99";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_96";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_97";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_98";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_100";
   attribute HU_SET of XLXI_19 : label is "XLXI_19_101";
   attribute HU_SET of XLXI_20 : label is "XLXI_20_102";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_103";
begin
   Q0 <= Q0_DUMMY;
   Q1 <= Q1_DUMMY;
   Q2 <= Q2_DUMMY;
   XLXI_1 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D0,
                Q=>XLXN_288);
   
   XLXI_2 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D1,
                Q=>XLXN_290);
   
   XLXI_3 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D2,
                Q=>XLXN_291);
   
   XLXI_4 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D3,
                Q=>XLXN_292);
   
   XLXI_18 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D5,
                Q=>Q5);
   
   XLXI_19 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D6,
                Q=>Q6);
   
   XLXI_20 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D7,
                Q=>Q7);
   
   XLXI_21 : FDC_MXILINX_Robot_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D4,
                Q=>Q4);
   
   XLXI_33 : AND2
      port map (I0=>D9,
                I1=>D8,
                O=>XLXN_285);
   
   XLXI_34 : NAND2
      port map (I0=>D11,
                I1=>D10,
                O=>XLXN_286);
   
   XLXI_41 : decoder_MUSER_Robot_V1
      port map (I0=>Q0_DUMMY,
                I1=>Q1_DUMMY,
                I2=>Q2_DUMMY,
                LM1=>Q8,
                LM2=>Q9,
                RM1=>Q10,
                RM2=>Q11);
   
   XLXI_43 : sipo_MUSER_Robot_V1
      port map (CLOCK=>XLXN_263,
                SERIAL_IN=>D8,
                Q0=>Q0_DUMMY,
                Q1=>Q1_DUMMY,
                Q2=>Q2_DUMMY);
   
   XLXI_45 : clk_dividor_MUSER_Robot_V1
      port map (CLK_IN=>XLXN_256,
                SLOW_CLK_OUT=>XLXN_263);
   
   XLXI_46 : clk_dividor_MUSER_Robot_V1
      port map (CLK_IN=>XLXN_257,
                SLOW_CLK_OUT=>XLXN_256);
   
   XLXI_47 : clk_dividor_MUSER_Robot_V1
      port map (CLK_IN=>CLK,
                SLOW_CLK_OUT=>XLXN_257);
   
   XLXI_48 : sensors_MUSER_Robot_V1
      port map (B0=>XLXN_278,
                B1=>XLXN_279,
                B2=>XLXN_280,
                B3=>XLXN_281,
                B4=>XLXN_272,
                B5=>XLXN_283,
                CLK=>XLXN_263,
                DATA_OUT=>Q12);
   
   XLXI_51 : INV
      port map (I=>D13,
                O=>XLXN_279);
   
   XLXI_53 : INV
      port map (I=>D15,
                O=>XLXN_281);
   
   XLXI_54 : INV
      port map (I=>D16,
                O=>XLXN_272);
   
   XLXI_56 : INV
      port map (I=>D14,
                O=>XLXN_280);
   
   XLXI_57 : INV
      port map (I=>D12,
                O=>XLXN_278);
   
   XLXI_58 : INV
      port map (I=>D17,
                O=>XLXN_283);
   
   XLXI_59 : OR2
      port map (I0=>XLXN_286,
                I1=>XLXN_285,
                O=>Q13);
   
   XLXI_60 : OR4
      port map (I0=>XLXN_292,
                I1=>XLXN_291,
                I2=>XLXN_290,
                I3=>XLXN_288,
                O=>Q3);
   
end BEHAVIORAL;


