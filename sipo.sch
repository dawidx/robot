<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLOCK" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="SERIAL_IN" />
        <signal name="Q2" />
        <signal name="XLXN_139" />
        <signal name="XLXN_192" />
        <signal name="XLXN_194" />
        <signal name="XLXN_199" />
        <signal name="XLXN_201" />
        <signal name="XLXN_202" />
        <signal name="XLXN_205" />
        <signal name="XLXN_209" />
        <signal name="XLXN_211" />
        <signal name="XLXN_212" />
        <signal name="XLXN_213" />
        <signal name="XLXN_214" />
        <signal name="XLXN_215" />
        <port polarity="Input" name="CLOCK" />
        <port polarity="Output" name="Q0" />
        <port polarity="Output" name="Q1" />
        <port polarity="Input" name="SERIAL_IN" />
        <port polarity="Output" name="Q2" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="xor3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="208" y1="-128" y2="-128" x1="256" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <arc ex="64" ey="-176" sx="64" sy="-80" r="56" cx="32" cy="-128" />
            <arc ex="128" ey="-176" sx="208" sy="-128" r="88" cx="132" cy="-88" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="64" y1="-80" y2="-80" x1="128" />
            <line x2="64" y1="-176" y2="-176" x1="128" />
            <arc ex="208" ey="-128" sx="128" sy="-80" r="88" cx="132" cy="-168" />
        </blockdef>
        <blockdef name="xnor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
            <circle r="8" cx="220" cy="-96" />
            <line x2="256" y1="-96" y2="-96" x1="228" />
            <line x2="60" y1="-28" y2="-28" x1="60" />
        </blockdef>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="SERIAL_IN" name="D" />
            <blockpin signalname="XLXN_199" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_199" name="D" />
            <blockpin signalname="XLXN_201" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_201" name="D" />
            <blockpin signalname="XLXN_202" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_202" name="D" />
            <blockpin signalname="XLXN_214" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_35">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_214" name="D" />
            <blockpin signalname="XLXN_139" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_36">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_139" name="D" />
            <blockpin signalname="XLXN_213" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_37">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_213" name="D" />
            <blockpin signalname="XLXN_215" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_38">
            <blockpin signalname="CLOCK" name="C" />
            <blockpin signalname="XLXN_215" name="D" />
            <blockpin signalname="XLXN_205" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_40">
            <blockpin signalname="XLXN_192" name="C" />
            <blockpin signalname="XLXN_215" name="D" />
            <blockpin signalname="Q2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_41">
            <blockpin signalname="XLXN_192" name="C" />
            <blockpin signalname="XLXN_213" name="D" />
            <blockpin signalname="Q1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_42">
            <blockpin signalname="XLXN_192" name="C" />
            <blockpin signalname="XLXN_139" name="D" />
            <blockpin signalname="Q0" name="Q" />
        </block>
        <block symbolname="and3" name="XLXI_60">
            <blockpin signalname="XLXN_199" name="I0" />
            <blockpin signalname="XLXN_201" name="I1" />
            <blockpin signalname="XLXN_202" name="I2" />
            <blockpin signalname="XLXN_194" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_61">
            <blockpin signalname="XLXN_194" name="I0" />
            <blockpin signalname="XLXN_209" name="I1" />
            <blockpin signalname="XLXN_211" name="I2" />
            <blockpin signalname="XLXN_192" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_62">
            <blockpin signalname="XLXN_205" name="I" />
            <blockpin signalname="XLXN_209" name="O" />
        </block>
        <block symbolname="xor3" name="XLXI_64">
            <blockpin signalname="XLXN_215" name="I0" />
            <blockpin signalname="XLXN_213" name="I1" />
            <blockpin signalname="XLXN_139" name="I2" />
            <blockpin signalname="XLXN_212" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_80">
            <blockpin signalname="XLXN_214" name="I0" />
            <blockpin signalname="XLXN_212" name="I1" />
            <blockpin signalname="XLXN_211" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="656" y="800" name="XLXI_2" orien="R0" />
        <instance x="2352" y="800" name="XLXI_5" orien="R0" />
        <iomarker fontsize="28" x="416" y="800" name="CLOCK" orien="R180" />
        <instance x="1248" y="800" name="XLXI_3" orien="R0" />
        <instance x="1840" y="800" name="XLXI_4" orien="R0" />
        <iomarker fontsize="28" x="320" y="544" name="SERIAL_IN" orien="R180" />
        <branch name="SERIAL_IN">
            <wire x2="656" y1="544" y2="544" x1="320" />
        </branch>
        <instance x="2928" y="800" name="XLXI_35" orien="R0" />
        <instance x="3472" y="800" name="XLXI_36" orien="R0" />
        <instance x="4000" y="800" name="XLXI_37" orien="R0" />
        <instance x="4576" y="800" name="XLXI_38" orien="R0" />
        <branch name="Q2">
            <wire x2="4864" y1="1152" y2="1152" x1="4816" />
        </branch>
        <instance x="4432" y="1408" name="XLXI_40" orien="R0" />
        <instance x="3920" y="1408" name="XLXI_41" orien="R0" />
        <branch name="Q1">
            <wire x2="4320" y1="1152" y2="1152" x1="4304" />
            <wire x2="4320" y1="1152" y2="1200" x1="4320" />
            <wire x2="4352" y1="1200" y2="1200" x1="4320" />
        </branch>
        <iomarker fontsize="28" x="4352" y="1200" name="Q1" orien="R0" />
        <instance x="3376" y="1408" name="XLXI_42" orien="R0" />
        <branch name="XLXN_139">
            <wire x2="3312" y1="1200" y2="1200" x1="2928" />
            <wire x2="3408" y1="544" y2="544" x1="3312" />
            <wire x2="3472" y1="544" y2="544" x1="3408" />
            <wire x2="3408" y1="544" y2="1008" x1="3408" />
            <wire x2="3312" y1="1008" y2="1152" x1="3312" />
            <wire x2="3376" y1="1152" y2="1152" x1="3312" />
            <wire x2="3312" y1="1152" y2="1200" x1="3312" />
            <wire x2="3408" y1="1008" y2="1008" x1="3312" />
        </branch>
        <branch name="Q0">
            <wire x2="3776" y1="1152" y2="1152" x1="3760" />
            <wire x2="3776" y1="1152" y2="1200" x1="3776" />
            <wire x2="3856" y1="1200" y2="1200" x1="3776" />
        </branch>
        <branch name="XLXN_192">
            <wire x2="2976" y1="1232" y2="1232" x1="2608" />
            <wire x2="2976" y1="1232" y2="1280" x1="2976" />
            <wire x2="3376" y1="1280" y2="1280" x1="2976" />
            <wire x2="2976" y1="1280" y2="1408" x1="2976" />
            <wire x2="3824" y1="1408" y2="1408" x1="2976" />
            <wire x2="2976" y1="1408" y2="1424" x1="2976" />
            <wire x2="4384" y1="1424" y2="1424" x1="2976" />
            <wire x2="3824" y1="1280" y2="1408" x1="3824" />
            <wire x2="3920" y1="1280" y2="1280" x1="3824" />
            <wire x2="4432" y1="1280" y2="1280" x1="4384" />
            <wire x2="4384" y1="1280" y2="1424" x1="4384" />
        </branch>
        <iomarker fontsize="28" x="3856" y="1200" name="Q0" orien="R0" />
        <iomarker fontsize="28" x="4864" y="1152" name="Q2" orien="R0" />
        <instance x="1664" y="1408" name="XLXI_60" orien="R0" />
        <branch name="XLXN_199">
            <wire x2="1152" y1="544" y2="544" x1="1040" />
            <wire x2="1248" y1="544" y2="544" x1="1152" />
            <wire x2="1152" y1="544" y2="1344" x1="1152" />
            <wire x2="1664" y1="1344" y2="1344" x1="1152" />
        </branch>
        <branch name="XLXN_201">
            <wire x2="1600" y1="1152" y2="1280" x1="1600" />
            <wire x2="1664" y1="1280" y2="1280" x1="1600" />
            <wire x2="1728" y1="1152" y2="1152" x1="1600" />
            <wire x2="1728" y1="544" y2="544" x1="1632" />
            <wire x2="1840" y1="544" y2="544" x1="1728" />
            <wire x2="1728" y1="544" y2="1152" x1="1728" />
        </branch>
        <branch name="XLXN_202">
            <wire x2="2288" y1="864" y2="864" x1="1664" />
            <wire x2="1664" y1="864" y2="1216" x1="1664" />
            <wire x2="2288" y1="544" y2="544" x1="2224" />
            <wire x2="2352" y1="544" y2="544" x1="2288" />
            <wire x2="2288" y1="544" y2="864" x1="2288" />
        </branch>
        <instance x="2352" y="1360" name="XLXI_61" orien="R0" />
        <branch name="XLXN_194">
            <wire x2="1936" y1="1280" y2="1280" x1="1920" />
            <wire x2="1936" y1="1280" y2="1296" x1="1936" />
            <wire x2="2352" y1="1296" y2="1296" x1="1936" />
        </branch>
        <branch name="XLXN_209">
            <wire x2="2352" y1="1232" y2="1232" x1="2320" />
        </branch>
        <instance x="2096" y="1264" name="XLXI_62" orien="R0" />
        <branch name="XLXN_205">
            <wire x2="4976" y1="960" y2="960" x1="2016" />
            <wire x2="2016" y1="960" y2="1232" x1="2016" />
            <wire x2="2096" y1="1232" y2="1232" x1="2016" />
            <wire x2="4976" y1="544" y2="544" x1="4960" />
            <wire x2="4976" y1="544" y2="960" x1="4976" />
        </branch>
        <branch name="XLXN_211">
            <wire x2="2160" y1="1104" y2="1168" x1="2160" />
            <wire x2="2352" y1="1168" y2="1168" x1="2160" />
            <wire x2="2304" y1="1104" y2="1104" x1="2160" />
        </branch>
        <branch name="XLXN_212">
            <wire x2="2672" y1="1136" y2="1136" x1="2560" />
        </branch>
        <instance x="2928" y="1008" name="XLXI_64" orien="R180" />
        <branch name="XLXN_213">
            <wire x2="3360" y1="1136" y2="1136" x1="2928" />
            <wire x2="3360" y1="992" y2="1136" x1="3360" />
            <wire x2="3904" y1="992" y2="992" x1="3360" />
            <wire x2="3904" y1="992" y2="1152" x1="3904" />
            <wire x2="3920" y1="1152" y2="1152" x1="3904" />
            <wire x2="3904" y1="544" y2="544" x1="3856" />
            <wire x2="4000" y1="544" y2="544" x1="3904" />
            <wire x2="3904" y1="544" y2="992" x1="3904" />
        </branch>
        <branch name="XLXN_214">
            <wire x2="2640" y1="1072" y2="1072" x1="2560" />
            <wire x2="2640" y1="992" y2="1072" x1="2640" />
            <wire x2="2784" y1="992" y2="992" x1="2640" />
            <wire x2="2784" y1="544" y2="544" x1="2736" />
            <wire x2="2928" y1="544" y2="544" x1="2784" />
            <wire x2="2784" y1="544" y2="992" x1="2784" />
        </branch>
        <branch name="CLOCK">
            <wire x2="656" y1="800" y2="800" x1="416" />
            <wire x2="1248" y1="800" y2="800" x1="656" />
            <wire x2="1840" y1="800" y2="800" x1="1248" />
            <wire x2="2336" y1="800" y2="800" x1="1840" />
            <wire x2="2800" y1="800" y2="800" x1="2336" />
            <wire x2="3344" y1="800" y2="800" x1="2800" />
            <wire x2="3920" y1="800" y2="800" x1="3344" />
            <wire x2="4448" y1="800" y2="800" x1="3920" />
            <wire x2="656" y1="672" y2="672" x1="592" />
            <wire x2="592" y1="672" y2="784" x1="592" />
            <wire x2="656" y1="784" y2="784" x1="592" />
            <wire x2="656" y1="784" y2="800" x1="656" />
            <wire x2="1248" y1="672" y2="672" x1="1184" />
            <wire x2="1184" y1="672" y2="784" x1="1184" />
            <wire x2="1248" y1="784" y2="784" x1="1184" />
            <wire x2="1248" y1="784" y2="800" x1="1248" />
            <wire x2="1840" y1="672" y2="672" x1="1776" />
            <wire x2="1776" y1="672" y2="784" x1="1776" />
            <wire x2="1840" y1="784" y2="784" x1="1776" />
            <wire x2="1840" y1="784" y2="800" x1="1840" />
            <wire x2="2352" y1="672" y2="672" x1="2336" />
            <wire x2="2336" y1="672" y2="800" x1="2336" />
            <wire x2="2928" y1="672" y2="672" x1="2800" />
            <wire x2="2800" y1="672" y2="800" x1="2800" />
            <wire x2="3472" y1="672" y2="672" x1="3344" />
            <wire x2="3344" y1="672" y2="800" x1="3344" />
            <wire x2="4000" y1="672" y2="672" x1="3920" />
            <wire x2="3920" y1="672" y2="800" x1="3920" />
            <wire x2="4576" y1="672" y2="672" x1="4448" />
            <wire x2="4448" y1="672" y2="800" x1="4448" />
        </branch>
        <instance x="2560" y="1008" name="XLXI_80" orien="R180" />
        <branch name="XLXN_215">
            <wire x2="2928" y1="928" y2="1072" x1="2928" />
            <wire x2="4400" y1="928" y2="928" x1="2928" />
            <wire x2="4400" y1="928" y2="1152" x1="4400" />
            <wire x2="4432" y1="1152" y2="1152" x1="4400" />
            <wire x2="4400" y1="544" y2="544" x1="4384" />
            <wire x2="4400" y1="544" y2="928" x1="4400" />
            <wire x2="4576" y1="544" y2="544" x1="4400" />
        </branch>
    </sheet>
</drawing>