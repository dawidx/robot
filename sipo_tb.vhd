-- Vhdl test bench created from schematic H:\Robot\Robot\sipo.sch - Fri May 03 15:39:28 2013
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY sipo_sipo_sch_tb IS
END sipo_sipo_sch_tb;
ARCHITECTURE behavioral OF sipo_sipo_sch_tb IS 

   COMPONENT sipo
   PORT( CLOCK	:	IN	STD_LOGIC; 
          Q0	:	OUT	STD_LOGIC; 
          Q1	:	OUT	STD_LOGIC; 
          Q2	:	OUT	STD_LOGIC;
          SERIAL_IN	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL CLOCK	:	STD_LOGIC;
   SIGNAL Q0	:	STD_LOGIC;
   SIGNAL Q1	:	STD_LOGIC;
   SIGNAL Q2	:	STD_LOGIC;
   SIGNAL SERIAL_IN	:	STD_LOGIC;
BEGIN

   UUT: sipo PORT MAP(
		CLOCK => CLOCK, 
		Q0 => Q0, 
		Q1 => Q1, 
		Q2 => Q2,
		SERIAL_IN => SERIAL_IN 
   );
	
	CLK : PROCESS
	BEGIN
		LOOP
			CLOCK <= '1'; WAIT FOR 50ns;
			CLOCK <= '0'; WAIT FOR 50ns;
		END LOOP;
	END PROCESS;
	
	
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		SERIAL_IN <= '1';
		WAIT FOR 200ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '0';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		SERIAL_IN <= '1';
		WAIT FOR 100ns;
		
		
		
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
