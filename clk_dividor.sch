<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_10" />
        <signal name="SLOW_CLK_OUT" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="CLK_IN" />
        <port polarity="Output" name="SLOW_CLK_OUT" />
        <port polarity="Input" name="CLK_IN" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fd" name="XLXI_1">
            <blockpin signalname="CLK_IN" name="C" />
            <blockpin signalname="XLXN_2" name="D" />
            <blockpin signalname="XLXN_1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="XLXN_2" name="C" />
            <blockpin signalname="XLXN_4" name="D" />
            <blockpin signalname="XLXN_5" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="XLXN_4" name="C" />
            <blockpin signalname="XLXN_11" name="D" />
            <blockpin signalname="XLXN_7" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="XLXN_1" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="XLXN_5" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="XLXN_7" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="XLXN_11" name="C" />
            <blockpin signalname="SLOW_CLK_OUT" name="D" />
            <blockpin signalname="XLXN_9" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="XLXN_9" name="I" />
            <blockpin signalname="SLOW_CLK_OUT" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="560" y="832" name="XLXI_1" orien="R0" />
        <instance x="1040" y="832" name="XLXI_2" orien="R0" />
        <instance x="1552" y="832" name="XLXI_3" orien="R0" />
        <instance x="864" y="256" name="XLXI_5" orien="R180" />
        <instance x="1296" y="256" name="XLXI_6" orien="R180" />
        <instance x="1824" y="272" name="XLXI_7" orien="R180" />
        <branch name="XLXN_1">
            <wire x2="960" y1="288" y2="288" x1="864" />
            <wire x2="960" y1="288" y2="576" x1="960" />
            <wire x2="960" y1="576" y2="576" x1="944" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="544" y1="288" y2="576" x1="544" />
            <wire x2="560" y1="576" y2="576" x1="544" />
            <wire x2="624" y1="288" y2="288" x1="544" />
            <wire x2="640" y1="288" y2="288" x1="624" />
            <wire x2="1008" y1="192" y2="192" x1="624" />
            <wire x2="1008" y1="192" y2="704" x1="1008" />
            <wire x2="1040" y1="704" y2="704" x1="1008" />
            <wire x2="624" y1="192" y2="288" x1="624" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1024" y1="288" y2="576" x1="1024" />
            <wire x2="1040" y1="576" y2="576" x1="1024" />
            <wire x2="1056" y1="288" y2="288" x1="1024" />
            <wire x2="1072" y1="288" y2="288" x1="1056" />
            <wire x2="1056" y1="224" y2="288" x1="1056" />
            <wire x2="1520" y1="224" y2="224" x1="1056" />
            <wire x2="1520" y1="224" y2="704" x1="1520" />
            <wire x2="1552" y1="704" y2="704" x1="1520" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1440" y1="288" y2="288" x1="1296" />
            <wire x2="1440" y1="288" y2="576" x1="1440" />
            <wire x2="1440" y1="576" y2="576" x1="1424" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1952" y1="304" y2="304" x1="1824" />
            <wire x2="1952" y1="304" y2="576" x1="1952" />
            <wire x2="1952" y1="576" y2="576" x1="1936" />
        </branch>
        <branch name="SLOW_CLK_OUT">
            <wire x2="2032" y1="304" y2="576" x1="2032" />
            <wire x2="2048" y1="576" y2="576" x1="2032" />
            <wire x2="2048" y1="304" y2="304" x1="2032" />
            <wire x2="2064" y1="304" y2="304" x1="2048" />
            <wire x2="2640" y1="224" y2="224" x1="2048" />
            <wire x2="2640" y1="224" y2="720" x1="2640" />
            <wire x2="2048" y1="224" y2="304" x1="2048" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="2448" y1="304" y2="304" x1="2288" />
            <wire x2="2448" y1="304" y2="576" x1="2448" />
            <wire x2="2448" y1="576" y2="576" x1="2432" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1536" y1="304" y2="576" x1="1536" />
            <wire x2="1552" y1="576" y2="576" x1="1536" />
            <wire x2="1584" y1="304" y2="304" x1="1536" />
            <wire x2="1600" y1="304" y2="304" x1="1584" />
            <wire x2="1584" y1="240" y2="304" x1="1584" />
            <wire x2="2000" y1="240" y2="240" x1="1584" />
            <wire x2="2000" y1="240" y2="704" x1="2000" />
            <wire x2="2032" y1="704" y2="704" x1="2000" />
            <wire x2="2048" y1="704" y2="704" x1="2032" />
        </branch>
        <instance x="2048" y="832" name="XLXI_4" orien="R0" />
        <instance x="2288" y="272" name="XLXI_8" orien="R180" />
        <branch name="CLK_IN">
            <wire x2="560" y1="704" y2="704" x1="528" />
        </branch>
        <iomarker fontsize="28" x="528" y="704" name="CLK_IN" orien="R180" />
        <iomarker fontsize="28" x="2640" y="720" name="SLOW_CLK_OUT" orien="R90" />
    </sheet>
</drawing>