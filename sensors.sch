<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="XLXN_14" />
        <signal name="XLXN_36" />
        <signal name="XLXN_34" />
        <signal name="XLXN_33" />
        <signal name="XLXN_31" />
        <signal name="XLXN_30" />
        <signal name="XLXN_29" />
        <signal name="XLXN_28" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_35" />
        <signal name="XLXN_54" />
        <signal name="XLXN_95" />
        <signal name="DATA_OUT" />
        <signal name="XLXN_97" />
        <signal name="XLXN_98" />
        <signal name="XLXN_99" />
        <signal name="XLXN_100" />
        <signal name="XLXN_101" />
        <signal name="XLXN_104" />
        <signal name="XLXN_106" />
        <signal name="XLXN_108" />
        <signal name="XLXN_110" />
        <signal name="XLXN_112" />
        <signal name="XLXN_113" />
        <signal name="XLXN_115" />
        <signal name="XLXN_129" />
        <signal name="B5" />
        <signal name="B4" />
        <signal name="B3" />
        <signal name="B2" />
        <signal name="B1" />
        <signal name="B0" />
        <signal name="XLXN_138" />
        <signal name="XLXN_139" />
        <signal name="XLXN_141" />
        <signal name="XLXN_142" />
        <signal name="XLXN_143" />
        <signal name="XLXN_144" />
        <signal name="XLXN_145" />
        <signal name="XLXN_146" />
        <signal name="XLXN_147" />
        <signal name="XLXN_149" />
        <signal name="XLXN_157" />
        <signal name="XLXN_158" />
        <signal name="XLXN_159" />
        <signal name="XLXN_160" />
        <signal name="XLXN_161" />
        <signal name="XLXN_162" />
        <signal name="XLXN_176" />
        <signal name="XLXN_177" />
        <signal name="XLXN_178" />
        <signal name="XLXN_179" />
        <signal name="XLXN_180" />
        <signal name="XLXN_181" />
        <signal name="XLXN_183" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="DATA_OUT" />
        <port polarity="Input" name="B5" />
        <port polarity="Input" name="B4" />
        <port polarity="Input" name="B3" />
        <port polarity="Input" name="B2" />
        <port polarity="Input" name="B1" />
        <port polarity="Input" name="B0" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="m2_1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-96" x1="64" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="64" y1="-64" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="xor6">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <arc ex="208" ey="-224" sx="128" sy="-176" r="88" cx="132" cy="-264" />
            <arc ex="64" ey="-272" sx="64" sy="-176" r="56" cx="32" cy="-224" />
            <line x2="64" y1="-176" y2="-176" x1="128" />
            <line x2="64" y1="-272" y2="-272" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="48" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-320" y2="-320" x1="0" />
            <line x2="48" y1="-384" y2="-384" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <arc ex="48" ey="-272" sx="48" sy="-176" r="56" cx="16" cy="-224" />
            <line x2="48" y1="-64" y2="-176" x1="48" />
            <line x2="48" y1="-272" y2="-384" x1="48" />
            <arc ex="128" ey="-272" sx="208" sy="-224" r="88" cx="132" cy="-184" />
            <line x2="208" y1="-224" y2="-224" x1="256" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="96" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="nand4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="216" y1="-160" y2="-160" x1="256" />
            <circle r="12" cx="204" cy="-160" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="64" y1="-112" y2="-112" x1="144" />
        </blockdef>
        <block symbolname="fd" name="XLXI_16">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_54" name="D" />
            <blockpin signalname="XLXN_36" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_15">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_51" name="D" />
            <blockpin signalname="XLXN_35" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_14">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_50" name="D" />
            <blockpin signalname="XLXN_34" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_49" name="D" />
            <blockpin signalname="XLXN_33" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_12">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_48" name="D" />
            <blockpin signalname="XLXN_31" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_11">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_47" name="D" />
            <blockpin signalname="XLXN_30" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_10">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_14" name="D" />
            <blockpin signalname="XLXN_29" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_145" name="D" />
            <blockpin signalname="XLXN_28" name="Q" />
        </block>
        <block symbolname="m2_1" name="XLXI_22">
            <blockpin signalname="XLXN_139" name="D0" />
            <blockpin signalname="XLXN_34" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_51" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_21">
            <blockpin signalname="XLXN_144" name="D0" />
            <blockpin signalname="XLXN_33" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_20">
            <blockpin signalname="XLXN_143" name="D0" />
            <blockpin signalname="XLXN_31" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_49" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_19">
            <blockpin signalname="XLXN_142" name="D0" />
            <blockpin signalname="XLXN_30" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_48" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_18">
            <blockpin signalname="XLXN_141" name="D0" />
            <blockpin signalname="XLXN_29" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_17">
            <blockpin signalname="XLXN_145" name="D0" />
            <blockpin signalname="XLXN_28" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_23">
            <blockpin signalname="XLXN_138" name="D0" />
            <blockpin signalname="XLXN_35" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_54" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_40">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_115" name="D" />
            <blockpin signalname="DATA_OUT" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_41">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_112" name="D" />
            <blockpin signalname="XLXN_113" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_42">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_110" name="D" />
            <blockpin signalname="XLXN_97" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_43">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_108" name="D" />
            <blockpin signalname="XLXN_98" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_44">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_106" name="D" />
            <blockpin signalname="XLXN_99" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_45">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_104" name="D" />
            <blockpin signalname="XLXN_100" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_46">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_95" name="D" />
            <blockpin signalname="XLXN_101" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_47">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_36" name="D" />
            <blockpin signalname="XLXN_178" name="Q" />
        </block>
        <block symbolname="m2_1" name="XLXI_48">
            <blockpin signalname="B5" name="D0" />
            <blockpin signalname="XLXN_97" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_112" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_49">
            <blockpin signalname="B4" name="D0" />
            <blockpin signalname="XLXN_98" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_110" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_50">
            <blockpin signalname="B3" name="D0" />
            <blockpin signalname="XLXN_99" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_108" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_51">
            <blockpin signalname="B2" name="D0" />
            <blockpin signalname="XLXN_100" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_106" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_52">
            <blockpin signalname="B1" name="D0" />
            <blockpin signalname="XLXN_101" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_104" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_53">
            <blockpin signalname="B0" name="D0" />
            <blockpin signalname="XLXN_178" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_95" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_54">
            <blockpin signalname="XLXN_129" name="D0" />
            <blockpin signalname="XLXN_113" name="D1" />
            <blockpin signalname="XLXN_179" name="S0" />
            <blockpin signalname="XLXN_115" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_55">
            <blockpin signalname="XLXN_129" name="G" />
        </block>
        <block symbolname="xor6" name="XLXI_56">
            <blockpin signalname="B5" name="I0" />
            <blockpin signalname="B4" name="I1" />
            <blockpin signalname="B3" name="I2" />
            <blockpin signalname="B2" name="I3" />
            <blockpin signalname="B1" name="I4" />
            <blockpin signalname="B0" name="I5" />
            <blockpin signalname="XLXN_138" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_57">
            <blockpin signalname="XLXN_139" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_58">
            <blockpin signalname="XLXN_144" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_59">
            <blockpin signalname="XLXN_143" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_60">
            <blockpin signalname="XLXN_142" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_61">
            <blockpin signalname="XLXN_141" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_62">
            <blockpin signalname="XLXN_145" name="P" />
        </block>
        <block symbolname="fd" name="XLXI_63">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_147" name="D" />
            <blockpin signalname="XLXN_146" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_64">
            <blockpin signalname="XLXN_146" name="C" />
            <blockpin signalname="XLXN_157" name="D" />
            <blockpin signalname="XLXN_149" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_65">
            <blockpin signalname="XLXN_149" name="C" />
            <blockpin signalname="XLXN_159" name="D" />
            <blockpin signalname="XLXN_158" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_66">
            <blockpin signalname="XLXN_158" name="C" />
            <blockpin signalname="XLXN_160" name="D" />
            <blockpin signalname="XLXN_161" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_68">
            <blockpin signalname="XLXN_146" name="I" />
            <blockpin signalname="XLXN_147" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_69">
            <blockpin signalname="XLXN_149" name="I" />
            <blockpin signalname="XLXN_157" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_70">
            <blockpin signalname="XLXN_158" name="I" />
            <blockpin signalname="XLXN_159" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_71">
            <blockpin signalname="XLXN_161" name="I" />
            <blockpin signalname="XLXN_160" name="O" />
        </block>
        <block symbolname="nand4" name="XLXI_74">
            <blockpin signalname="XLXN_146" name="I0" />
            <blockpin signalname="XLXN_149" name="I1" />
            <blockpin signalname="XLXN_158" name="I2" />
            <blockpin signalname="XLXN_161" name="I3" />
            <blockpin signalname="XLXN_179" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <attr value="Inch" name="LengthUnitName" />
        <attr value="10" name="GridsPerUnit" />
        <instance x="3744" y="1312" name="XLXI_16" orien="R0" />
        <instance x="3264" y="1312" name="XLXI_15" orien="R0" />
        <instance x="2784" y="1312" name="XLXI_14" orien="R0" />
        <instance x="2304" y="1312" name="XLXI_13" orien="R0" />
        <instance x="1824" y="1312" name="XLXI_12" orien="R0" />
        <instance x="1344" y="1312" name="XLXI_11" orien="R0" />
        <instance x="864" y="1312" name="XLXI_10" orien="R0" />
        <branch name="XLXN_14">
            <wire x2="848" y1="928" y2="1056" x1="848" />
            <wire x2="864" y1="1056" y2="1056" x1="848" />
            <wire x2="992" y1="928" y2="928" x1="848" />
            <wire x2="992" y1="720" y2="720" x1="912" />
            <wire x2="992" y1="720" y2="928" x1="992" />
        </branch>
        <instance x="352" y="1312" name="XLXI_9" orien="R0" />
        <branch name="XLXN_36">
            <wire x2="4608" y1="1552" y2="1552" x1="304" />
            <wire x2="304" y1="1552" y2="2240" x1="304" />
            <wire x2="384" y1="2240" y2="2240" x1="304" />
            <wire x2="4608" y1="1056" y2="1056" x1="4128" />
            <wire x2="4608" y1="1056" y2="1552" x1="4608" />
        </branch>
        <instance x="3136" y="848" name="XLXI_22" orien="R0" />
        <branch name="XLXN_34">
            <wire x2="3136" y1="752" y2="752" x1="3072" />
            <wire x2="3072" y1="752" y2="880" x1="3072" />
            <wire x2="3184" y1="880" y2="880" x1="3072" />
            <wire x2="3184" y1="880" y2="1056" x1="3184" />
            <wire x2="3184" y1="1056" y2="1056" x1="3168" />
        </branch>
        <instance x="2656" y="848" name="XLXI_21" orien="R0" />
        <branch name="XLXN_33">
            <wire x2="2656" y1="752" y2="752" x1="2592" />
            <wire x2="2592" y1="752" y2="880" x1="2592" />
            <wire x2="2704" y1="880" y2="880" x1="2592" />
            <wire x2="2704" y1="880" y2="1056" x1="2704" />
            <wire x2="2704" y1="1056" y2="1056" x1="2688" />
        </branch>
        <instance x="2096" y="848" name="XLXI_20" orien="R0" />
        <branch name="XLXN_31">
            <wire x2="2096" y1="752" y2="752" x1="2016" />
            <wire x2="2016" y1="752" y2="880" x1="2016" />
            <wire x2="2224" y1="880" y2="880" x1="2016" />
            <wire x2="2224" y1="880" y2="1056" x1="2224" />
            <wire x2="2224" y1="1056" y2="1056" x1="2208" />
        </branch>
        <instance x="1616" y="848" name="XLXI_19" orien="R0" />
        <branch name="XLXN_30">
            <wire x2="1616" y1="752" y2="752" x1="1552" />
            <wire x2="1552" y1="752" y2="880" x1="1552" />
            <wire x2="1744" y1="880" y2="880" x1="1552" />
            <wire x2="1744" y1="880" y2="1056" x1="1744" />
            <wire x2="1744" y1="1056" y2="1056" x1="1728" />
        </branch>
        <instance x="1152" y="848" name="XLXI_18" orien="R0" />
        <branch name="XLXN_29">
            <wire x2="1152" y1="752" y2="752" x1="1088" />
            <wire x2="1088" y1="752" y2="880" x1="1088" />
            <wire x2="1264" y1="880" y2="880" x1="1088" />
            <wire x2="1264" y1="880" y2="1056" x1="1264" />
            <wire x2="1264" y1="1056" y2="1056" x1="1248" />
        </branch>
        <instance x="592" y="848" name="XLXI_17" orien="R0" />
        <branch name="XLXN_28">
            <wire x2="592" y1="752" y2="752" x1="528" />
            <wire x2="528" y1="752" y2="880" x1="528" />
            <wire x2="752" y1="880" y2="880" x1="528" />
            <wire x2="752" y1="880" y2="1056" x1="752" />
            <wire x2="752" y1="1056" y2="1056" x1="736" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="1280" y1="912" y2="1056" x1="1280" />
            <wire x2="1344" y1="1056" y2="1056" x1="1280" />
            <wire x2="1536" y1="912" y2="912" x1="1280" />
            <wire x2="1536" y1="720" y2="720" x1="1472" />
            <wire x2="1536" y1="720" y2="912" x1="1536" />
        </branch>
        <branch name="XLXN_48">
            <wire x2="1760" y1="928" y2="1056" x1="1760" />
            <wire x2="1824" y1="1056" y2="1056" x1="1760" />
            <wire x2="2000" y1="928" y2="928" x1="1760" />
            <wire x2="2000" y1="720" y2="720" x1="1936" />
            <wire x2="2000" y1="720" y2="928" x1="2000" />
        </branch>
        <branch name="XLXN_49">
            <wire x2="2240" y1="928" y2="1056" x1="2240" />
            <wire x2="2304" y1="1056" y2="1056" x1="2240" />
            <wire x2="2496" y1="928" y2="928" x1="2240" />
            <wire x2="2496" y1="720" y2="720" x1="2416" />
            <wire x2="2496" y1="720" y2="928" x1="2496" />
        </branch>
        <branch name="XLXN_50">
            <wire x2="2720" y1="928" y2="1056" x1="2720" />
            <wire x2="2784" y1="1056" y2="1056" x1="2720" />
            <wire x2="3040" y1="928" y2="928" x1="2720" />
            <wire x2="3040" y1="720" y2="720" x1="2976" />
            <wire x2="3040" y1="720" y2="928" x1="3040" />
        </branch>
        <branch name="XLXN_51">
            <wire x2="3200" y1="928" y2="1056" x1="3200" />
            <wire x2="3264" y1="1056" y2="1056" x1="3200" />
            <wire x2="3536" y1="928" y2="928" x1="3200" />
            <wire x2="3536" y1="720" y2="720" x1="3456" />
            <wire x2="3536" y1="720" y2="928" x1="3536" />
        </branch>
        <instance x="3664" y="848" name="XLXI_23" orien="R0" />
        <branch name="XLXN_35">
            <wire x2="3664" y1="752" y2="752" x1="3600" />
            <wire x2="3600" y1="752" y2="880" x1="3600" />
            <wire x2="3664" y1="880" y2="880" x1="3600" />
            <wire x2="3664" y1="880" y2="1056" x1="3664" />
            <wire x2="3664" y1="1056" y2="1056" x1="3648" />
        </branch>
        <branch name="XLXN_54">
            <wire x2="3680" y1="1056" y2="1312" x1="3680" />
            <wire x2="4192" y1="1312" y2="1312" x1="3680" />
            <wire x2="3744" y1="1056" y2="1056" x1="3680" />
            <wire x2="4192" y1="720" y2="720" x1="3984" />
            <wire x2="4192" y1="720" y2="1312" x1="4192" />
        </branch>
        <instance x="3776" y="2496" name="XLXI_40" orien="R0" />
        <instance x="3296" y="2496" name="XLXI_41" orien="R0" />
        <instance x="2816" y="2496" name="XLXI_42" orien="R0" />
        <instance x="2336" y="2496" name="XLXI_43" orien="R0" />
        <instance x="1856" y="2496" name="XLXI_44" orien="R0" />
        <instance x="1376" y="2496" name="XLXI_45" orien="R0" />
        <instance x="896" y="2496" name="XLXI_46" orien="R0" />
        <branch name="XLXN_95">
            <wire x2="880" y1="2112" y2="2240" x1="880" />
            <wire x2="896" y1="2240" y2="2240" x1="880" />
            <wire x2="1024" y1="2112" y2="2112" x1="880" />
            <wire x2="1024" y1="1904" y2="1904" x1="944" />
            <wire x2="1024" y1="1904" y2="2112" x1="1024" />
        </branch>
        <instance x="384" y="2496" name="XLXI_47" orien="R0" />
        <branch name="DATA_OUT">
            <wire x2="4640" y1="2240" y2="2240" x1="4160" />
        </branch>
        <instance x="3168" y="2032" name="XLXI_48" orien="R0" />
        <branch name="XLXN_97">
            <wire x2="3168" y1="1936" y2="1936" x1="3104" />
            <wire x2="3104" y1="1936" y2="2064" x1="3104" />
            <wire x2="3216" y1="2064" y2="2064" x1="3104" />
            <wire x2="3216" y1="2064" y2="2240" x1="3216" />
            <wire x2="3216" y1="2240" y2="2240" x1="3200" />
        </branch>
        <instance x="2688" y="2032" name="XLXI_49" orien="R0" />
        <branch name="XLXN_98">
            <wire x2="2688" y1="1936" y2="1936" x1="2624" />
            <wire x2="2624" y1="1936" y2="2064" x1="2624" />
            <wire x2="2736" y1="2064" y2="2064" x1="2624" />
            <wire x2="2736" y1="2064" y2="2240" x1="2736" />
            <wire x2="2736" y1="2240" y2="2240" x1="2720" />
        </branch>
        <instance x="2128" y="2032" name="XLXI_50" orien="R0" />
        <branch name="XLXN_99">
            <wire x2="2128" y1="1936" y2="1936" x1="2048" />
            <wire x2="2048" y1="1936" y2="2064" x1="2048" />
            <wire x2="2256" y1="2064" y2="2064" x1="2048" />
            <wire x2="2256" y1="2064" y2="2240" x1="2256" />
            <wire x2="2256" y1="2240" y2="2240" x1="2240" />
        </branch>
        <instance x="1648" y="2032" name="XLXI_51" orien="R0" />
        <branch name="XLXN_100">
            <wire x2="1648" y1="1936" y2="1936" x1="1584" />
            <wire x2="1584" y1="1936" y2="2064" x1="1584" />
            <wire x2="1776" y1="2064" y2="2064" x1="1584" />
            <wire x2="1776" y1="2064" y2="2240" x1="1776" />
            <wire x2="1776" y1="2240" y2="2240" x1="1760" />
        </branch>
        <instance x="1184" y="2032" name="XLXI_52" orien="R0" />
        <branch name="XLXN_101">
            <wire x2="1184" y1="1936" y2="1936" x1="1120" />
            <wire x2="1120" y1="1936" y2="2064" x1="1120" />
            <wire x2="1296" y1="2064" y2="2064" x1="1120" />
            <wire x2="1296" y1="2064" y2="2240" x1="1296" />
            <wire x2="1296" y1="2240" y2="2240" x1="1280" />
        </branch>
        <instance x="624" y="2032" name="XLXI_53" orien="R0" />
        <branch name="XLXN_104">
            <wire x2="1312" y1="2096" y2="2240" x1="1312" />
            <wire x2="1376" y1="2240" y2="2240" x1="1312" />
            <wire x2="1568" y1="2096" y2="2096" x1="1312" />
            <wire x2="1568" y1="1904" y2="1904" x1="1504" />
            <wire x2="1568" y1="1904" y2="2096" x1="1568" />
        </branch>
        <branch name="XLXN_106">
            <wire x2="1792" y1="2112" y2="2240" x1="1792" />
            <wire x2="1856" y1="2240" y2="2240" x1="1792" />
            <wire x2="2032" y1="2112" y2="2112" x1="1792" />
            <wire x2="2032" y1="1904" y2="1904" x1="1968" />
            <wire x2="2032" y1="1904" y2="2112" x1="2032" />
        </branch>
        <branch name="XLXN_108">
            <wire x2="2272" y1="2112" y2="2240" x1="2272" />
            <wire x2="2336" y1="2240" y2="2240" x1="2272" />
            <wire x2="2528" y1="2112" y2="2112" x1="2272" />
            <wire x2="2528" y1="1904" y2="1904" x1="2448" />
            <wire x2="2528" y1="1904" y2="2112" x1="2528" />
        </branch>
        <branch name="XLXN_110">
            <wire x2="2752" y1="2112" y2="2240" x1="2752" />
            <wire x2="2816" y1="2240" y2="2240" x1="2752" />
            <wire x2="3072" y1="2112" y2="2112" x1="2752" />
            <wire x2="3072" y1="1904" y2="1904" x1="3008" />
            <wire x2="3072" y1="1904" y2="2112" x1="3072" />
        </branch>
        <branch name="XLXN_112">
            <wire x2="3232" y1="2112" y2="2240" x1="3232" />
            <wire x2="3296" y1="2240" y2="2240" x1="3232" />
            <wire x2="3568" y1="2112" y2="2112" x1="3232" />
            <wire x2="3568" y1="1904" y2="1904" x1="3488" />
            <wire x2="3568" y1="1904" y2="2112" x1="3568" />
        </branch>
        <instance x="3696" y="2032" name="XLXI_54" orien="R0" />
        <branch name="XLXN_113">
            <wire x2="3696" y1="1936" y2="1936" x1="3632" />
            <wire x2="3632" y1="1936" y2="2064" x1="3632" />
            <wire x2="3696" y1="2064" y2="2064" x1="3632" />
            <wire x2="3696" y1="2064" y2="2240" x1="3696" />
            <wire x2="3696" y1="2240" y2="2240" x1="3680" />
        </branch>
        <branch name="XLXN_115">
            <wire x2="3776" y1="2240" y2="2240" x1="3712" />
            <wire x2="3712" y1="2240" y2="2496" x1="3712" />
            <wire x2="4224" y1="2496" y2="2496" x1="3712" />
            <wire x2="4224" y1="1904" y2="1904" x1="4016" />
            <wire x2="4224" y1="1904" y2="2496" x1="4224" />
        </branch>
        <iomarker fontsize="28" x="5280" y="1392" name="CLK" orien="R0" />
        <instance x="3696" y="1744" name="XLXI_55" orien="R180" />
        <branch name="XLXN_129">
            <wire x2="3632" y1="1872" y2="1920" x1="3632" />
            <wire x2="3664" y1="1920" y2="1920" x1="3632" />
            <wire x2="3664" y1="1872" y2="1920" x1="3664" />
            <wire x2="3696" y1="1872" y2="1872" x1="3664" />
        </branch>
        <branch name="B5">
            <wire x2="3152" y1="1664" y2="1664" x1="3120" />
            <wire x2="3152" y1="1664" y2="1872" x1="3152" />
            <wire x2="3168" y1="1872" y2="1872" x1="3152" />
            <wire x2="3120" y1="1664" y2="1744" x1="3120" />
            <wire x2="4272" y1="1744" y2="1744" x1="3120" />
            <wire x2="3168" y1="1632" y2="1632" x1="3152" />
            <wire x2="3152" y1="1632" y2="1664" x1="3152" />
            <wire x2="4272" y1="192" y2="192" x1="3888" />
            <wire x2="4272" y1="192" y2="1744" x1="4272" />
        </branch>
        <branch name="B4">
            <wire x2="2688" y1="1632" y2="1632" x1="2672" />
            <wire x2="2672" y1="1632" y2="1680" x1="2672" />
            <wire x2="2672" y1="1680" y2="1872" x1="2672" />
            <wire x2="2688" y1="1872" y2="1872" x1="2672" />
            <wire x2="4288" y1="1680" y2="1680" x1="2672" />
            <wire x2="4288" y1="256" y2="256" x1="3888" />
            <wire x2="4288" y1="256" y2="1680" x1="4288" />
        </branch>
        <branch name="B3">
            <wire x2="2096" y1="1712" y2="1792" x1="2096" />
            <wire x2="2112" y1="1792" y2="1792" x1="2096" />
            <wire x2="2112" y1="1792" y2="1872" x1="2112" />
            <wire x2="2128" y1="1872" y2="1872" x1="2112" />
            <wire x2="4144" y1="1712" y2="1712" x1="2096" />
            <wire x2="2128" y1="1632" y2="1632" x1="2112" />
            <wire x2="2112" y1="1632" y2="1792" x1="2112" />
            <wire x2="4144" y1="320" y2="320" x1="3888" />
            <wire x2="4144" y1="320" y2="1712" x1="4144" />
        </branch>
        <branch name="B2">
            <wire x2="1648" y1="1632" y2="1632" x1="1632" />
            <wire x2="1632" y1="1632" y2="1760" x1="1632" />
            <wire x2="1632" y1="1760" y2="1872" x1="1632" />
            <wire x2="1648" y1="1872" y2="1872" x1="1632" />
            <wire x2="4160" y1="1760" y2="1760" x1="1632" />
            <wire x2="4160" y1="384" y2="384" x1="3888" />
            <wire x2="4160" y1="384" y2="1760" x1="4160" />
        </branch>
        <branch name="B1">
            <wire x2="1184" y1="1632" y2="1632" x1="1168" />
            <wire x2="1168" y1="1632" y2="1728" x1="1168" />
            <wire x2="1168" y1="1728" y2="1872" x1="1168" />
            <wire x2="1184" y1="1872" y2="1872" x1="1168" />
            <wire x2="4176" y1="1728" y2="1728" x1="1168" />
            <wire x2="4176" y1="448" y2="448" x1="3888" />
            <wire x2="4176" y1="448" y2="1728" x1="4176" />
        </branch>
        <branch name="B0">
            <wire x2="624" y1="1632" y2="1632" x1="608" />
            <wire x2="608" y1="1632" y2="1696" x1="608" />
            <wire x2="608" y1="1696" y2="1872" x1="608" />
            <wire x2="624" y1="1872" y2="1872" x1="608" />
            <wire x2="4208" y1="1696" y2="1696" x1="608" />
            <wire x2="4208" y1="512" y2="512" x1="3888" />
            <wire x2="4208" y1="512" y2="1696" x1="4208" />
        </branch>
        <iomarker fontsize="28" x="624" y="1632" name="B0" orien="R0" />
        <iomarker fontsize="28" x="1184" y="1632" name="B1" orien="R0" />
        <iomarker fontsize="28" x="1648" y="1632" name="B2" orien="R0" />
        <iomarker fontsize="28" x="2128" y="1632" name="B3" orien="R0" />
        <iomarker fontsize="28" x="2688" y="1632" name="B4" orien="R0" />
        <iomarker fontsize="28" x="3168" y="1632" name="B5" orien="R0" />
        <instance x="3888" y="128" name="XLXI_56" orien="R180" />
        <branch name="XLXN_138">
            <wire x2="3632" y1="352" y2="352" x1="3616" />
            <wire x2="3616" y1="352" y2="688" x1="3616" />
            <wire x2="3664" y1="688" y2="688" x1="3616" />
        </branch>
        <branch name="XLXN_139">
            <wire x2="3120" y1="640" y2="640" x1="3072" />
            <wire x2="3120" y1="640" y2="688" x1="3120" />
            <wire x2="3136" y1="688" y2="688" x1="3120" />
        </branch>
        <instance x="3072" y="704" name="XLXI_57" orien="R270" />
        <instance x="2624" y="704" name="XLXI_58" orien="R270" />
        <instance x="2096" y="704" name="XLXI_59" orien="R270" />
        <instance x="1120" y="720" name="XLXI_61" orien="R270" />
        <instance x="592" y="704" name="XLXI_62" orien="R270" />
        <branch name="XLXN_141">
            <wire x2="1136" y1="656" y2="656" x1="1120" />
            <wire x2="1136" y1="656" y2="688" x1="1136" />
            <wire x2="1152" y1="688" y2="688" x1="1136" />
        </branch>
        <instance x="1584" y="704" name="XLXI_60" orien="R270" />
        <branch name="XLXN_142">
            <wire x2="1600" y1="640" y2="640" x1="1584" />
            <wire x2="1600" y1="640" y2="688" x1="1600" />
            <wire x2="1616" y1="688" y2="688" x1="1600" />
        </branch>
        <branch name="XLXN_143">
            <wire x2="2160" y1="544" y2="544" x1="1968" />
            <wire x2="2160" y1="544" y2="640" x1="2160" />
            <wire x2="1968" y1="544" y2="688" x1="1968" />
            <wire x2="2096" y1="688" y2="688" x1="1968" />
            <wire x2="2160" y1="640" y2="640" x1="2096" />
        </branch>
        <branch name="XLXN_144">
            <wire x2="2640" y1="640" y2="640" x1="2624" />
            <wire x2="2640" y1="640" y2="688" x1="2640" />
            <wire x2="2656" y1="688" y2="688" x1="2640" />
        </branch>
        <branch name="XLXN_145">
            <wire x2="304" y1="704" y2="1056" x1="304" />
            <wire x2="352" y1="1056" y2="1056" x1="304" />
            <wire x2="528" y1="704" y2="704" x1="304" />
            <wire x2="656" y1="544" y2="544" x1="464" />
            <wire x2="656" y1="544" y2="640" x1="656" />
            <wire x2="464" y1="544" y2="688" x1="464" />
            <wire x2="528" y1="688" y2="688" x1="464" />
            <wire x2="592" y1="688" y2="688" x1="528" />
            <wire x2="528" y1="688" y2="704" x1="528" />
            <wire x2="656" y1="640" y2="640" x1="592" />
        </branch>
        <instance x="960" y="3152" name="XLXI_64" orien="R0" />
        <instance x="1488" y="3152" name="XLXI_65" orien="R0" />
        <instance x="2032" y="3152" name="XLXI_66" orien="R0" />
        <instance x="704" y="2624" name="XLXI_68" orien="R180" />
        <instance x="1248" y="2624" name="XLXI_69" orien="R180" />
        <instance x="1792" y="2640" name="XLXI_70" orien="R180" />
        <instance x="2320" y="2640" name="XLXI_71" orien="R180" />
        <branch name="XLXN_147">
            <wire x2="480" y1="2656" y2="2656" x1="400" />
            <wire x2="400" y1="2656" y2="2896" x1="400" />
        </branch>
        <branch name="XLXN_149">
            <wire x2="1392" y1="3328" y2="3328" x1="528" />
            <wire x2="1360" y1="2656" y2="2656" x1="1248" />
            <wire x2="1360" y1="2656" y2="2896" x1="1360" />
            <wire x2="1360" y1="2896" y2="3024" x1="1360" />
            <wire x2="1392" y1="3024" y2="3024" x1="1360" />
            <wire x2="1488" y1="3024" y2="3024" x1="1392" />
            <wire x2="1392" y1="3024" y2="3328" x1="1392" />
            <wire x2="1360" y1="2896" y2="2896" x1="1344" />
        </branch>
        <branch name="XLXN_146">
            <wire x2="832" y1="3264" y2="3264" x1="528" />
            <wire x2="816" y1="2656" y2="2656" x1="704" />
            <wire x2="816" y1="2656" y2="2896" x1="816" />
            <wire x2="816" y1="2896" y2="3024" x1="816" />
            <wire x2="832" y1="3024" y2="3024" x1="816" />
            <wire x2="960" y1="3024" y2="3024" x1="832" />
            <wire x2="832" y1="3024" y2="3264" x1="832" />
            <wire x2="816" y1="2896" y2="2896" x1="784" />
        </branch>
        <branch name="XLXN_157">
            <wire x2="944" y1="2656" y2="2896" x1="944" />
            <wire x2="960" y1="2896" y2="2896" x1="944" />
            <wire x2="1024" y1="2656" y2="2656" x1="944" />
        </branch>
        <branch name="XLXN_158">
            <wire x2="1968" y1="3392" y2="3392" x1="528" />
            <wire x2="1952" y1="2672" y2="2672" x1="1792" />
            <wire x2="1952" y1="2672" y2="2896" x1="1952" />
            <wire x2="1952" y1="2896" y2="3024" x1="1952" />
            <wire x2="1968" y1="3024" y2="3024" x1="1952" />
            <wire x2="2032" y1="3024" y2="3024" x1="1968" />
            <wire x2="1968" y1="3024" y2="3392" x1="1968" />
            <wire x2="1952" y1="2896" y2="2896" x1="1872" />
        </branch>
        <branch name="XLXN_159">
            <wire x2="1472" y1="2672" y2="2896" x1="1472" />
            <wire x2="1488" y1="2896" y2="2896" x1="1472" />
            <wire x2="1568" y1="2672" y2="2672" x1="1472" />
        </branch>
        <branch name="XLXN_160">
            <wire x2="2016" y1="2672" y2="2896" x1="2016" />
            <wire x2="2032" y1="2896" y2="2896" x1="2016" />
            <wire x2="2096" y1="2672" y2="2672" x1="2016" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="2432" y1="3456" y2="3456" x1="528" />
            <wire x2="2432" y1="2672" y2="2672" x1="2320" />
            <wire x2="2432" y1="2672" y2="2896" x1="2432" />
            <wire x2="2432" y1="2896" y2="3456" x1="2432" />
            <wire x2="2432" y1="2896" y2="2896" x1="2416" />
        </branch>
        <branch name="CLK">
            <wire x2="352" y1="1184" y2="1184" x1="320" />
            <wire x2="320" y1="1184" y2="1392" x1="320" />
            <wire x2="832" y1="1392" y2="1392" x1="320" />
            <wire x2="1328" y1="1392" y2="1392" x1="832" />
            <wire x2="1792" y1="1392" y2="1392" x1="1328" />
            <wire x2="2288" y1="1392" y2="1392" x1="1792" />
            <wire x2="2768" y1="1392" y2="1392" x1="2288" />
            <wire x2="3248" y1="1392" y2="1392" x1="2768" />
            <wire x2="3728" y1="1392" y2="1392" x1="3248" />
            <wire x2="4928" y1="1392" y2="1392" x1="3728" />
            <wire x2="5280" y1="1392" y2="1392" x1="4928" />
            <wire x2="4928" y1="1392" y2="2480" x1="4928" />
            <wire x2="384" y1="2368" y2="2368" x1="352" />
            <wire x2="352" y1="2368" y2="2480" x1="352" />
            <wire x2="864" y1="2480" y2="2480" x1="352" />
            <wire x2="1344" y1="2480" y2="2480" x1="864" />
            <wire x2="1824" y1="2480" y2="2480" x1="1344" />
            <wire x2="2288" y1="2480" y2="2480" x1="1824" />
            <wire x2="2768" y1="2480" y2="2480" x1="2288" />
            <wire x2="3264" y1="2480" y2="2480" x1="2768" />
            <wire x2="3728" y1="2480" y2="2480" x1="3264" />
            <wire x2="4928" y1="2480" y2="2480" x1="3728" />
            <wire x2="352" y1="2480" y2="3024" x1="352" />
            <wire x2="400" y1="3024" y2="3024" x1="352" />
            <wire x2="864" y1="1184" y2="1184" x1="832" />
            <wire x2="832" y1="1184" y2="1392" x1="832" />
            <wire x2="896" y1="2368" y2="2368" x1="864" />
            <wire x2="864" y1="2368" y2="2480" x1="864" />
            <wire x2="1344" y1="1184" y2="1184" x1="1328" />
            <wire x2="1328" y1="1184" y2="1392" x1="1328" />
            <wire x2="1376" y1="2368" y2="2368" x1="1344" />
            <wire x2="1344" y1="2368" y2="2480" x1="1344" />
            <wire x2="1824" y1="1184" y2="1184" x1="1792" />
            <wire x2="1792" y1="1184" y2="1392" x1="1792" />
            <wire x2="1856" y1="2368" y2="2368" x1="1824" />
            <wire x2="1824" y1="2368" y2="2480" x1="1824" />
            <wire x2="2304" y1="1184" y2="1184" x1="2288" />
            <wire x2="2288" y1="1184" y2="1392" x1="2288" />
            <wire x2="2336" y1="2368" y2="2368" x1="2288" />
            <wire x2="2288" y1="2368" y2="2480" x1="2288" />
            <wire x2="2784" y1="1184" y2="1184" x1="2768" />
            <wire x2="2768" y1="1184" y2="1392" x1="2768" />
            <wire x2="2816" y1="2368" y2="2368" x1="2768" />
            <wire x2="2768" y1="2368" y2="2480" x1="2768" />
            <wire x2="3264" y1="1184" y2="1184" x1="3248" />
            <wire x2="3248" y1="1184" y2="1392" x1="3248" />
            <wire x2="3296" y1="2368" y2="2368" x1="3264" />
            <wire x2="3264" y1="2368" y2="2480" x1="3264" />
            <wire x2="3744" y1="1184" y2="1184" x1="3728" />
            <wire x2="3728" y1="1184" y2="1392" x1="3728" />
            <wire x2="3776" y1="2368" y2="2368" x1="3728" />
            <wire x2="3728" y1="2368" y2="2480" x1="3728" />
        </branch>
        <instance x="400" y="3152" name="XLXI_63" orien="R0" />
        <iomarker fontsize="28" x="4640" y="2240" name="DATA_OUT" orien="R0" />
        <branch name="XLXN_178">
            <wire x2="624" y1="1936" y2="1936" x1="560" />
            <wire x2="560" y1="1936" y2="2064" x1="560" />
            <wire x2="784" y1="2064" y2="2064" x1="560" />
            <wire x2="784" y1="2064" y2="2240" x1="784" />
            <wire x2="784" y1="2240" y2="2240" x1="768" />
        </branch>
        <branch name="XLXN_179">
            <wire x2="336" y1="2048" y2="2048" x1="240" />
            <wire x2="624" y1="2048" y2="2048" x1="336" />
            <wire x2="1008" y1="2048" y2="2048" x1="624" />
            <wire x2="1648" y1="2048" y2="2048" x1="1008" />
            <wire x2="2128" y1="2048" y2="2048" x1="1648" />
            <wire x2="2672" y1="2048" y2="2048" x1="2128" />
            <wire x2="3168" y1="2048" y2="2048" x1="2672" />
            <wire x2="3696" y1="2048" y2="2048" x1="3168" />
            <wire x2="240" y1="2048" y2="3360" x1="240" />
            <wire x2="272" y1="3360" y2="3360" x1="240" />
            <wire x2="592" y1="864" y2="864" x1="336" />
            <wire x2="976" y1="864" y2="864" x1="592" />
            <wire x2="1616" y1="864" y2="864" x1="976" />
            <wire x2="2080" y1="864" y2="864" x1="1616" />
            <wire x2="2656" y1="864" y2="864" x1="2080" />
            <wire x2="3136" y1="864" y2="864" x1="2656" />
            <wire x2="3664" y1="864" y2="864" x1="3136" />
            <wire x2="336" y1="864" y2="2048" x1="336" />
            <wire x2="592" y1="816" y2="864" x1="592" />
            <wire x2="624" y1="2000" y2="2048" x1="624" />
            <wire x2="1152" y1="816" y2="816" x1="976" />
            <wire x2="976" y1="816" y2="864" x1="976" />
            <wire x2="1184" y1="2000" y2="2000" x1="1008" />
            <wire x2="1008" y1="2000" y2="2048" x1="1008" />
            <wire x2="1616" y1="816" y2="864" x1="1616" />
            <wire x2="1648" y1="2000" y2="2048" x1="1648" />
            <wire x2="2096" y1="816" y2="816" x1="2080" />
            <wire x2="2080" y1="816" y2="864" x1="2080" />
            <wire x2="2128" y1="2000" y2="2048" x1="2128" />
            <wire x2="2656" y1="816" y2="864" x1="2656" />
            <wire x2="2688" y1="2000" y2="2000" x1="2672" />
            <wire x2="2672" y1="2000" y2="2048" x1="2672" />
            <wire x2="3136" y1="816" y2="864" x1="3136" />
            <wire x2="3168" y1="2000" y2="2048" x1="3168" />
            <wire x2="3664" y1="816" y2="864" x1="3664" />
            <wire x2="3696" y1="2000" y2="2048" x1="3696" />
        </branch>
        <instance x="528" y="3200" name="XLXI_74" orien="R180" />
    </sheet>
</drawing>